
#include <ros/ros.h>
#include "sensor_msgs/JointState.h"
#include <custom_msgs/pendulum_status.h>
//#include <custom_msgs/area_status.h>
#include <string>
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include "serial/serial.h"
#define PI 3.14159265358979323846  
#define DATASIZE 6
#define Forward 1
#define Reverse 0 
#define ShowValue 1 //0 for show armCallback ROS_INFO, 1 for show segwayCallback ROS_INFO, 

std::string port("/dev/ttyUSB0");
unsigned long baud = 19200;
serial::Serial my_serial(port, baud, serial::Timeout::simpleTimeout(2000));

size_t bytes_wrote; 
float refVal_list[2][6] = {0.0};
unsigned char uart_transmit_list[91];  //(2 * 5 * 9 ) +1, 2 arm, 5 joint/arm, 1 joint degree/9 unit
volatile bool armCallback_finish = 0;

double rounding(double, int);

//FMSC 相對型 計算角度給手臂 
typedef struct DC {
  //DC() {};
  float controlVal ;  
  float error;
  float controlMax;

  float fuzzy_input_rule[11];
  float rule[11];
  float fsmcvector[11];

  float lamda;
  float lamda_d;
  float lowerWidth;
  float gs;
  float gu;
  float gu_rel_scale;
  float sliding_value;
};

DC motorL1 = {  // initVariable()也要記得改
  0.0, 0.0, 0.45,
  {-1.0, -0.8, -0.6, -0.3, -0.1, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0 }, 
  //min: 1.0*0.2*0.1*200=1.8deg/sec, 108deg/60sec, 0.3rpm
  //max: 1.0*0.2*1.0*200=18deg/sec, 1080/60sec, 3rpm
  {-1.0, -0.8, -0.6, -0.3, -0.1, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0 }, 
  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
  3.0, 0.1, 0.0, 18.0, 0.0, 0.2, 0.0
};
float FSMC_ArmThetaCMD_calculate( float* , float* , float* , DC* );
volatile float arm_bal_ref = 0.0;
float refVel = 18.0;  //18rpm, 用rosparam set動態設定
float refOdm = 227.419;  //227.4197001 cm, 18rpm走10秒的大概距離, 用rosparam set動態設定
volatile float vel_old_L = 0.0;
volatile float vel_old_R = 0.0;
volatile uint64_t cnt_isr_Xms = 0; //2^16 * 0.005 /60/60 約等於25,620,477,880,152hr已內不會溢位
//預留給image 辨識面積用
volatile float segway_theta = 0.0; //用PID來簡單測試傳值而已
volatile bool area_locating = 0;
volatile size_t go_FR = -1;  //Forward = 1, Reverse = 0

void armCallback(const sensor_msgs::JointState &msg)  //約10Hz
{
  double theta;  //degree 
  for (int arm_No = 1; arm_No >= 0; arm_No--)  //重新排列使陣列索引與每個關節對應, 且其排列、代表索引與Nios II專案(接收端)的程式相同
  {
    for (int joint_No = 1; joint_No <=5; joint_No++)
    {
      if (arm_No == 1)
      {        
        theta = (180.0*msg.position[joint_No-1])/PI;  //rad->degree
        theta = rounding(theta, 2);  //取小數點後兩位
        refVal_list[arm_No][joint_No] = theta;
      }
      else
      {
        theta = (180.0*msg.position[joint_No+4])/PI;
        theta = rounding(theta, 2);  //取小數點後兩位
        refVal_list[arm_No][joint_No] = theta;
      }
      //ROS_INFO("refVal_list[%d][%d] %f", arm_No, joint_No, refVal_list[arm_No][joint_No]);
    }
  }


  refVal_list[0][1] = rounding(arm_bal_ref, 2);  //取小數點後兩位;  //左手第一軸

  char buffer[6];  //最多6byte(含小數點) XXX.XX, 不考慮+-的情況下
  size_t length = 0;
  char clength = 0;
  char *pch;
  char cplus_or_negative = '0';
  long int float_index = 0;
  char cfloat_index = '0';
  memset(buffer,0,sizeof(buffer));
  memset(uart_transmit_list,0,sizeof(uart_transmit_list));
  int scan_idx = 0;
  for (int arm_No = 0; arm_No <= 1; arm_No++)
  {
    for (int joint_No = 1; joint_No <= 5; joint_No++)
    {      
      if (refVal_list[arm_No][joint_No] > 0.0)      
        cplus_or_negative = '+';      
      else if (refVal_list[arm_No][joint_No] < 0.0)      
        cplus_or_negative = '-';
      if (ShowValue == 0)
      {
        ROS_INFO("arm_No[%d], joint_No[%d]", arm_No, joint_No);
        ROS_INFO("plus_or_negative %c", cplus_or_negative);        
      }
      snprintf(buffer, sizeof(buffer), "%f", fabs(refVal_list[arm_No][joint_No]));
      if (ShowValue == 0)      
        ROS_INFO("buffer %s", buffer);

      length = sizeof(buffer)/sizeof(char);  //如果想改成char* , https://stackoverflow.com/questions/21022644/how-to-get-the-real-and-total-length-of-char-char-array
      //ROS_INFO("legnth %zu", length);
      clength = (char)length + 48;
      //ROS_INFO("clegnth %c", clength);
                   
      pch=strchr(buffer,'.');
      while (pch!=NULL)
      {
        float_index = pch - buffer;
        //ROS_INFO("float index %ld", float_index);      
        cfloat_index = (char)float_index + 48;
        //ROS_INFO("float index char %c", cfloat_index);        
        pch=strchr(pch+1,'.');
      }

       uart_transmit_list[scan_idx] = clength;
       //ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
       scan_idx += 1;
       uart_transmit_list[scan_idx] = cplus_or_negative;
       //ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
       scan_idx += 1;
       uart_transmit_list[scan_idx] = cfloat_index;
       //ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
       scan_idx += 1;

      for(int i = 0; i < DATASIZE; i++)
      {
        if (buffer[i] == '\0')
        {
          buffer[i] = '0';
        }
        uart_transmit_list[scan_idx] = buffer[i];
        //ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
        scan_idx += 1;
      }
      //ROS_INFO("uart_transmit_list \n");        
    }
  }  

  uart_transmit_list[scan_idx] = '!';
  if (ShowValue == 0)  
    ROS_INFO("uart_transmit_list end , %d\n", scan_idx);
  armCallback_finish = 1;
}

void segwayCallback(const custom_msgs::pendulum_status &msg)  //約200Hz, 實際看msg.time約100Hz
{ 
  //float refVel = msg.refVel;  //當時custom msg時忘記定義了
  // ros::param::get("refVel", refVel);  //放這才不會只執行一次
  // refVel = static_cast<float>(refVel);  
  // ros::param::get("refOdm", refOdm);  //放這才不會只執行一次
  // refOdm = static_cast<float>(refOdm);  

  cnt_isr_Xms += 1;
  float odm_L = msg.odm_L;
  float odm_R = msg.odm_R;
  float vel_L = msg.vel_L;
  float vel_R = msg.vel_R;
  float acc_L = (vel_L - vel_old_L) * 5.0; //  /0.2, 最慢4.8rpm約0.2秒一定會更新, 但此值應該要隨速度增加成長才對
  float acc_R = (vel_R - vel_old_R) * 5.0; //  /0.2
  float avg_odm = (odm_L + odm_R) * 0.5;//  /2.0;   
  float avg_vel = (vel_L + vel_R) * 0.5;
  float avg_acc = (acc_L + acc_R) * 0.5; 
  
  float u = FSMC_ArmThetaCMD_calculate(&refVel, &avg_vel, &avg_acc, &motorL1);

  if (cnt_isr_Xms <= 2000)  //開始此節點後的20秒左右進行速度控制, 0.01s*2000 = 20, 根據打指令的速度有可能平衡控制會超過6秒
  {
    if (cnt_isr_Xms == 1) //first  
    {
      if (u >= 0)
      { 
        arm_bal_ref = motorL1.lowerWidth + u; 
      }
      else        
      { 
        arm_bal_ref = -motorL1.lowerWidth + u; 
      } 
    }
    else  
    {
      arm_bal_ref = arm_bal_ref + u;   
    }    
  }
  else
  {
    refVel = 0.0;
    refOdm = 227.419;
  }
  
  // arm_bal_ref = 0.03*(refVel-avg_vel);
  // arm_bal_ref += arm_bal_ref;

  if (arm_bal_ref >= 70.0)
    arm_bal_ref = 70.0;
  else if(arm_bal_ref <= -70.0)
    arm_bal_ref = -70.0;
    

  if (ShowValue == 1)  
  {
    ROS_INFO("%.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f", 
              msg.time, msg.angle_filter, msg.angular_velocity_rc, arm_bal_ref, 
              msg.LcontrolB, msg.vel_L, msg.vel_R, msg.odm_L, 
              msg.odm_R, msg.Lsliding_valueB, motorL1.sliding_value);
  }

  vel_old_L = vel_L;
  vel_old_R = vel_R;       
}
/*
void areaCallback(const custom_msgs::area_status &msg)  //約15HZ(default), 可在common.yaml調整 (zed_ros_ws), 現為60Hz
{
  if (((msg.area >= XXXX) && (msg.area <= XXXX)) && (abs(segway_theta) <= 2.0))
  {
    area_locating = 1;
    go_FR = -1;
  }
  else if ((msg.area >= XXXX) && (segway_theta >= 2.0))  //太近, 請往後
  {
    area_locating = 0;
    go_FR = Reverse;
  }
  else if ((msg.area <= XXXX) && (segway_theta <= 2.0))  //太遠, 請往前
  {
    area_locating = 0;
    go_FR = Forward;
  }
  else
  {
    ROS_INFO("Oops, somthing happened in cal_area node")
  }
}
*/
int main(int argc, char **argv)  
{
  ros::init(argc, argv, "fpga_node");
  ros::NodeHandle n;
  // ros::param::set("refVel", 0);
  // ros::param::set("refOdm", 0);
  ros::Subscriber subArm = n.subscribe("/joint_states", 1000, armCallback);
  ros::Subscriber subSegway = n.subscribe("/pendulum_status", 1000, segwayCallback);
  //ros::Subscriber subArea = n.subscribe("/area_status", 1000, areaCallback);
    
  std::cout << "Is the serial port open?";
  if(my_serial.isOpen())
    std::cout << " Yes." << std::endl;
  else
    std::cout << " No." << std::endl;

  ros::Rate loop_rate(10);
  while(ros::ok())
  {            
    ros::spinOnce();     
    loop_rate.sleep();    
    if (armCallback_finish)
    {
      serial::Timeout to = serial::Timeout::simpleTimeout(2000);
      my_serial.setTimeout(to);
      bytes_wrote = my_serial.write(uart_transmit_list, 91); 
      armCallback_finish = 0;
    }
  }

  my_serial.close();
  std::cout << " close !!!!!!!!!!!!!!!." << std::endl;
  
  return 0;
}
// 四捨五入 取到 小數點第 X 位 
double rounding(double num, int index)
{
    bool isNegative = false; // whether is negative number or not
  
    if(num < 0) // if this number is negative, then convert to positive number
    {
        isNegative = true;  
        num = -num;
    }
  
    if(index >= 0)
    {
        int multiplier;
        multiplier = pow(10, index);
        num = (int)(num * multiplier + 0.5) / (multiplier * 1.0);
    }
  
    if(isNegative) // if this number is negative, then convert to negative number
    {
        num = -num;
    }
  
    return num;
}

float FSMC_ArmThetaCMD_calculate( float* ref, float* pre, float* err_change, DC* motor) {
  float err = *ref - *pre;  //現在的速度小於理想速度手臂前擺(正角度)
  float err_d = *err_change;
  float gs = motor->gs;
  float gu = motor->gu_rel_scale * motor->controlMax;  //每次變化量為controlMax 的 XX %
  float lamda = motor->lamda;
  float lamda_d = motor->lamda_d;
  float x = 0.0;

  for (int i=0; i<11; i++)
    motor->fsmcvector[i] = 0.0;

  float *fuzzy_input_rule = &motor->fuzzy_input_rule[0]; 
  float *rule = &motor->rule[0];  

  long double sliding_value = lamda*err + lamda_d*err_d ;
  x=sliding_value/gs;  

  if (sliding_value >= fuzzy_input_rule[10] * gs) {
    motor->fsmcvector[10] = 1.0;
  }
  else if (sliding_value >= fuzzy_input_rule[9]*gs && sliding_value <= fuzzy_input_rule[10]*gs) {
    motor->fsmcvector[10] = (x-fuzzy_input_rule[9])/(fuzzy_input_rule[10]-fuzzy_input_rule[9]);
    motor->fsmcvector[9] = 1.0-motor->fsmcvector[10];
  }
  else if (sliding_value >= fuzzy_input_rule[8]*gs && sliding_value <= fuzzy_input_rule[9]*gs) {
    motor->fsmcvector[9] = (x-fuzzy_input_rule[8])/(fuzzy_input_rule[9]-fuzzy_input_rule[8]);
    motor->fsmcvector[8] = 1.0-motor->fsmcvector[9];
  }
  else if (sliding_value >= fuzzy_input_rule[7]*gs && sliding_value <= fuzzy_input_rule[8]*gs) {
    motor->fsmcvector[8] = (x-fuzzy_input_rule[7])/(fuzzy_input_rule[8]-fuzzy_input_rule[7]);
    motor->fsmcvector[7] = 1.0-motor->fsmcvector[8];
  }
  else if (sliding_value >= fuzzy_input_rule[6]*gs && sliding_value <= fuzzy_input_rule[7]*gs) {
    motor->fsmcvector[7] = (x-fuzzy_input_rule[6])/(fuzzy_input_rule[7]-fuzzy_input_rule[6]);
    motor->fsmcvector[6] = 1.0-motor->fsmcvector[7];
  }
  else if (sliding_value >= fuzzy_input_rule[5]*gs && sliding_value <= fuzzy_input_rule[6]*gs) {
    motor->fsmcvector[6] = (x-fuzzy_input_rule[5])/(fuzzy_input_rule[6]-fuzzy_input_rule[5]);
    motor->fsmcvector[5] = 1.0-motor->fsmcvector[6];
  }
  else if (sliding_value >= fuzzy_input_rule[4]*gs && sliding_value <= fuzzy_input_rule[5]*gs) {
    motor->fsmcvector[5] = (x-fuzzy_input_rule[4])/(fuzzy_input_rule[5]-fuzzy_input_rule[4]);
    motor->fsmcvector[4] = 1.0-motor->fsmcvector[5];
  }
  else if (sliding_value >= fuzzy_input_rule[3]*gs && sliding_value <= fuzzy_input_rule[4]*gs) {
    motor->fsmcvector[4] = (x-fuzzy_input_rule[3])/(fuzzy_input_rule[4]-fuzzy_input_rule[3]);
    motor->fsmcvector[3] = 1.0-motor->fsmcvector[4];
  }
  else if (sliding_value >= fuzzy_input_rule[2]*gs && sliding_value <= fuzzy_input_rule[3]*gs) {
    motor->fsmcvector[3] = (x-fuzzy_input_rule[2])/(fuzzy_input_rule[3]-fuzzy_input_rule[2]);
    motor->fsmcvector[2] = 1.0-motor->fsmcvector[3];
  }
  else if (sliding_value >= fuzzy_input_rule[1]*gs && sliding_value <= fuzzy_input_rule[2]*gs) {
    motor->fsmcvector[2] = (x-fuzzy_input_rule[1])/(fuzzy_input_rule[2]-fuzzy_input_rule[1]);
    motor->fsmcvector[1] = 1.0-motor->fsmcvector[2];
  }
  else if (sliding_value >= fuzzy_input_rule[0]*gs && sliding_value <= fuzzy_input_rule[1]*gs) {
    motor->fsmcvector[1] = (x-fuzzy_input_rule[0])/(fuzzy_input_rule[1]-fuzzy_input_rule[0]);
    motor->fsmcvector[0] = 1.0-motor->fsmcvector[1];
  }
  else if (sliding_value <= -fuzzy_input_rule[0] * gs) {
    motor->fsmcvector[0] = 1.0;
  }  

  float weight = 0.0;
  float weight_sum = 0.0;
  for (int i = 0; i < 11; i++) {
    if (motor->fsmcvector[i] > 0.0) {
      weight += motor->fsmcvector[i];
      // weight_sum += (motor->fsmcvector[i]) * rule[i];
      weight_sum = fma(motor->fsmcvector[i],rule[i],weight_sum);
    }
  }  
   

  float output;
  output = (float)(weight_sum * gu / weight);  //--2020/10/6 bugㄏㄏ  會變整數  不過實際也只能整數  所以這裡只影響plot時
  // ROS_INFO("%.7f, %.7f, %.7f, %.7f", weight, weight_sum, gu, output); 
    
  if (output >= motor->controlMax) 
    output =  motor->controlMax;
  else if (output <= -motor->controlMax) 
    output = -motor->controlMax;  

  motor->error = err;  //the same with preError = error;  //放外面不行不知為啥
  motor->controlVal = output;
  motor->sliding_value = sliding_value;  

  return output;
}
