# CMake generated Testfile for 
# Source directory: /home/nvidia/tx2_fpga_ws/src
# Build directory: /home/nvidia/tx2_fpga_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(serial)
subdirs(custom_msgs)
subdirs(fpga)
