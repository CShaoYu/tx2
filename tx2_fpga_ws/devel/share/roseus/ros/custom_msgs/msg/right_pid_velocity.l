;; Auto-generated. Do not edit!


(when (boundp 'custom_msgs::right_pid_velocity)
  (if (not (find-package "CUSTOM_MSGS"))
    (make-package "CUSTOM_MSGS"))
  (shadow 'right_pid_velocity (find-package "CUSTOM_MSGS")))
(unless (find-package "CUSTOM_MSGS::RIGHT_PID_VELOCITY")
  (make-package "CUSTOM_MSGS::RIGHT_PID_VELOCITY"))

(in-package "ROS")
;;//! \htmlinclude right_pid_velocity.msg.html


(defclass custom_msgs::right_pid_velocity
  :super ros::object
  :slots (_kp _ki _kd _controlMax ))

(defmethod custom_msgs::right_pid_velocity
  (:init
   (&key
    ((:kp __kp) 0.0)
    ((:ki __ki) 0.0)
    ((:kd __kd) 0.0)
    ((:controlMax __controlMax) 0.0)
    )
   (send-super :init)
   (setq _kp (float __kp))
   (setq _ki (float __ki))
   (setq _kd (float __kd))
   (setq _controlMax (float __controlMax))
   self)
  (:kp
   (&optional __kp)
   (if __kp (setq _kp __kp)) _kp)
  (:ki
   (&optional __ki)
   (if __ki (setq _ki __ki)) _ki)
  (:kd
   (&optional __kd)
   (if __kd (setq _kd __kd)) _kd)
  (:controlMax
   (&optional __controlMax)
   (if __controlMax (setq _controlMax __controlMax)) _controlMax)
  (:serialization-length
   ()
   (+
    ;; float32 _kp
    4
    ;; float32 _ki
    4
    ;; float32 _kd
    4
    ;; float32 _controlMax
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _kp
       (sys::poke _kp (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ki
       (sys::poke _ki (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _kd
       (sys::poke _kd (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _controlMax
       (sys::poke _controlMax (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _kp
     (setq _kp (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ki
     (setq _ki (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _kd
     (setq _kd (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _controlMax
     (setq _controlMax (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get custom_msgs::right_pid_velocity :md5sum-) "623d8ff803e92a174b2a9e53a920d15c")
(setf (get custom_msgs::right_pid_velocity :datatype-) "custom_msgs/right_pid_velocity")
(setf (get custom_msgs::right_pid_velocity :definition-)
      "float32 kp
float32 ki
float32 kd
float32 controlMax

")



(provide :custom_msgs/right_pid_velocity "623d8ff803e92a174b2a9e53a920d15c")


