;; Auto-generated. Do not edit!


(when (boundp 'custom_msgs::pwmVal_refRPM)
  (if (not (find-package "CUSTOM_MSGS"))
    (make-package "CUSTOM_MSGS"))
  (shadow 'pwmVal_refRPM (find-package "CUSTOM_MSGS")))
(unless (find-package "CUSTOM_MSGS::PWMVAL_REFRPM")
  (make-package "CUSTOM_MSGS::PWMVAL_REFRPM"))

(in-package "ROS")
;;//! \htmlinclude pwmVal_refRPM.msg.html


(defclass custom_msgs::pwmVal_refRPM
  :super ros::object
  :slots (_pwmVal _refRPM ))

(defmethod custom_msgs::pwmVal_refRPM
  (:init
   (&key
    ((:pwmVal __pwmVal) 0)
    ((:refRPM __refRPM) 0)
    )
   (send-super :init)
   (setq _pwmVal (round __pwmVal))
   (setq _refRPM (round __refRPM))
   self)
  (:pwmVal
   (&optional __pwmVal)
   (if __pwmVal (setq _pwmVal __pwmVal)) _pwmVal)
  (:refRPM
   (&optional __refRPM)
   (if __refRPM (setq _refRPM __refRPM)) _refRPM)
  (:serialization-length
   ()
   (+
    ;; int16 _pwmVal
    2
    ;; int16 _refRPM
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int16 _pwmVal
       (write-word _pwmVal s)
     ;; int16 _refRPM
       (write-word _refRPM s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int16 _pwmVal
     (setq _pwmVal (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _refRPM
     (setq _refRPM (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get custom_msgs::pwmVal_refRPM :md5sum-) "a65cdb1254a070de0d815cb99d141f87")
(setf (get custom_msgs::pwmVal_refRPM :datatype-) "custom_msgs/pwmVal_refRPM")
(setf (get custom_msgs::pwmVal_refRPM :definition-)
      "int16 pwmVal
int16 refRPM



")



(provide :custom_msgs/pwmVal_refRPM "a65cdb1254a070de0d815cb99d141f87")


