;; Auto-generated. Do not edit!


(when (boundp 'custom_msgs::left_fsmc_balance)
  (if (not (find-package "CUSTOM_MSGS"))
    (make-package "CUSTOM_MSGS"))
  (shadow 'left_fsmc_balance (find-package "CUSTOM_MSGS")))
(unless (find-package "CUSTOM_MSGS::LEFT_FSMC_BALANCE")
  (make-package "CUSTOM_MSGS::LEFT_FSMC_BALANCE"))

(in-package "ROS")
;;//! \htmlinclude left_fsmc_balance.msg.html


(defclass custom_msgs::left_fsmc_balance
  :super ros::object
  :slots (_lamda _gs1 _gs2 _gs3 _gu1 _gu2 _gu3 _controlMax ))

(defmethod custom_msgs::left_fsmc_balance
  (:init
   (&key
    ((:lamda __lamda) 0.0)
    ((:gs1 __gs1) 0.0)
    ((:gs2 __gs2) 0.0)
    ((:gs3 __gs3) 0.0)
    ((:gu1 __gu1) 0.0)
    ((:gu2 __gu2) 0.0)
    ((:gu3 __gu3) 0.0)
    ((:controlMax __controlMax) 0.0)
    )
   (send-super :init)
   (setq _lamda (float __lamda))
   (setq _gs1 (float __gs1))
   (setq _gs2 (float __gs2))
   (setq _gs3 (float __gs3))
   (setq _gu1 (float __gu1))
   (setq _gu2 (float __gu2))
   (setq _gu3 (float __gu3))
   (setq _controlMax (float __controlMax))
   self)
  (:lamda
   (&optional __lamda)
   (if __lamda (setq _lamda __lamda)) _lamda)
  (:gs1
   (&optional __gs1)
   (if __gs1 (setq _gs1 __gs1)) _gs1)
  (:gs2
   (&optional __gs2)
   (if __gs2 (setq _gs2 __gs2)) _gs2)
  (:gs3
   (&optional __gs3)
   (if __gs3 (setq _gs3 __gs3)) _gs3)
  (:gu1
   (&optional __gu1)
   (if __gu1 (setq _gu1 __gu1)) _gu1)
  (:gu2
   (&optional __gu2)
   (if __gu2 (setq _gu2 __gu2)) _gu2)
  (:gu3
   (&optional __gu3)
   (if __gu3 (setq _gu3 __gu3)) _gu3)
  (:controlMax
   (&optional __controlMax)
   (if __controlMax (setq _controlMax __controlMax)) _controlMax)
  (:serialization-length
   ()
   (+
    ;; float32 _lamda
    4
    ;; float32 _gs1
    4
    ;; float32 _gs2
    4
    ;; float32 _gs3
    4
    ;; float32 _gu1
    4
    ;; float32 _gu2
    4
    ;; float32 _gu3
    4
    ;; float32 _controlMax
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _lamda
       (sys::poke _lamda (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gs1
       (sys::poke _gs1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gs2
       (sys::poke _gs2 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gs3
       (sys::poke _gs3 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gu1
       (sys::poke _gu1 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gu2
       (sys::poke _gu2 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _gu3
       (sys::poke _gu3 (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _controlMax
       (sys::poke _controlMax (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _lamda
     (setq _lamda (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gs1
     (setq _gs1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gs2
     (setq _gs2 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gs3
     (setq _gs3 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gu1
     (setq _gu1 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gu2
     (setq _gu2 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _gu3
     (setq _gu3 (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _controlMax
     (setq _controlMax (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get custom_msgs::left_fsmc_balance :md5sum-) "dcef653360ac32e30e928872b8672b09")
(setf (get custom_msgs::left_fsmc_balance :datatype-) "custom_msgs/left_fsmc_balance")
(setf (get custom_msgs::left_fsmc_balance :definition-)
      "float32 lamda
float32 gs1
float32 gs2
float32 gs3
float32 gu1
float32 gu2
float32 gu3
float32 controlMax

")



(provide :custom_msgs/left_fsmc_balance "dcef653360ac32e30e928872b8672b09")


