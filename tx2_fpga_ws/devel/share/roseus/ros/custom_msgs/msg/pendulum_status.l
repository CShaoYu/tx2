;; Auto-generated. Do not edit!


(when (boundp 'custom_msgs::pendulum_status)
  (if (not (find-package "CUSTOM_MSGS"))
    (make-package "CUSTOM_MSGS"))
  (shadow 'pendulum_status (find-package "CUSTOM_MSGS")))
(unless (find-package "CUSTOM_MSGS::PENDULUM_STATUS")
  (make-package "CUSTOM_MSGS::PENDULUM_STATUS"))

(in-package "ROS")
;;//! \htmlinclude pendulum_status.msg.html


(defclass custom_msgs::pendulum_status
  :super ros::object
  :slots (_time _angle_acc _angle_gyro _angle_filter _scnt _swave _pwmVal _refOdm _rpm_L _rpm_R _rpm_L_tmp _rpm_R_tmp _dis_L_tmp _dis_R_tmp _odm_L _odm_R _locating _vel_L _vel_R _vel_L_tmp _vel_R_tmp _switch_count_pulse_L _switch_count_pulse_R _LrefV _RrefV _LerrV _RerrV _LcontrolV _RcontrolV _total_ctrl_L _total_ctrl_R _LRrefB _LRerrB _LcontrolB _RcontrolB ))

(defmethod custom_msgs::pendulum_status
  (:init
   (&key
    ((:time __time) 0.0)
    ((:angle_acc __angle_acc) 0.0)
    ((:angle_gyro __angle_gyro) 0.0)
    ((:angle_filter __angle_filter) 0.0)
    ((:scnt __scnt) 0.0)
    ((:swave __swave) 0.0)
    ((:pwmVal __pwmVal) 0)
    ((:refOdm __refOdm) 0.0)
    ((:rpm_L __rpm_L) 0)
    ((:rpm_R __rpm_R) 0)
    ((:rpm_L_tmp __rpm_L_tmp) 0)
    ((:rpm_R_tmp __rpm_R_tmp) 0)
    ((:dis_L_tmp __dis_L_tmp) 0)
    ((:dis_R_tmp __dis_R_tmp) 0)
    ((:odm_L __odm_L) 0.0)
    ((:odm_R __odm_R) 0.0)
    ((:locating __locating) nil)
    ((:vel_L __vel_L) 0.0)
    ((:vel_R __vel_R) 0.0)
    ((:vel_L_tmp __vel_L_tmp) 0)
    ((:vel_R_tmp __vel_R_tmp) 0)
    ((:switch_count_pulse_L __switch_count_pulse_L) 0)
    ((:switch_count_pulse_R __switch_count_pulse_R) 0)
    ((:LrefV __LrefV) 0)
    ((:RrefV __RrefV) 0)
    ((:LerrV __LerrV) 0.0)
    ((:RerrV __RerrV) 0.0)
    ((:LcontrolV __LcontrolV) 0.0)
    ((:RcontrolV __RcontrolV) 0.0)
    ((:total_ctrl_L __total_ctrl_L) 0.0)
    ((:total_ctrl_R __total_ctrl_R) 0.0)
    ((:LRrefB __LRrefB) 0.0)
    ((:LRerrB __LRerrB) 0.0)
    ((:LcontrolB __LcontrolB) 0.0)
    ((:RcontrolB __RcontrolB) 0.0)
    )
   (send-super :init)
   (setq _time (float __time))
   (setq _angle_acc (float __angle_acc))
   (setq _angle_gyro (float __angle_gyro))
   (setq _angle_filter (float __angle_filter))
   (setq _scnt (float __scnt))
   (setq _swave (float __swave))
   (setq _pwmVal (round __pwmVal))
   (setq _refOdm (float __refOdm))
   (setq _rpm_L (round __rpm_L))
   (setq _rpm_R (round __rpm_R))
   (setq _rpm_L_tmp (round __rpm_L_tmp))
   (setq _rpm_R_tmp (round __rpm_R_tmp))
   (setq _dis_L_tmp (round __dis_L_tmp))
   (setq _dis_R_tmp (round __dis_R_tmp))
   (setq _odm_L (float __odm_L))
   (setq _odm_R (float __odm_R))
   (setq _locating __locating)
   (setq _vel_L (float __vel_L))
   (setq _vel_R (float __vel_R))
   (setq _vel_L_tmp (round __vel_L_tmp))
   (setq _vel_R_tmp (round __vel_R_tmp))
   (setq _switch_count_pulse_L (round __switch_count_pulse_L))
   (setq _switch_count_pulse_R (round __switch_count_pulse_R))
   (setq _LrefV (round __LrefV))
   (setq _RrefV (round __RrefV))
   (setq _LerrV (float __LerrV))
   (setq _RerrV (float __RerrV))
   (setq _LcontrolV (float __LcontrolV))
   (setq _RcontrolV (float __RcontrolV))
   (setq _total_ctrl_L (float __total_ctrl_L))
   (setq _total_ctrl_R (float __total_ctrl_R))
   (setq _LRrefB (float __LRrefB))
   (setq _LRerrB (float __LRerrB))
   (setq _LcontrolB (float __LcontrolB))
   (setq _RcontrolB (float __RcontrolB))
   self)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:angle_acc
   (&optional __angle_acc)
   (if __angle_acc (setq _angle_acc __angle_acc)) _angle_acc)
  (:angle_gyro
   (&optional __angle_gyro)
   (if __angle_gyro (setq _angle_gyro __angle_gyro)) _angle_gyro)
  (:angle_filter
   (&optional __angle_filter)
   (if __angle_filter (setq _angle_filter __angle_filter)) _angle_filter)
  (:scnt
   (&optional __scnt)
   (if __scnt (setq _scnt __scnt)) _scnt)
  (:swave
   (&optional __swave)
   (if __swave (setq _swave __swave)) _swave)
  (:pwmVal
   (&optional __pwmVal)
   (if __pwmVal (setq _pwmVal __pwmVal)) _pwmVal)
  (:refOdm
   (&optional __refOdm)
   (if __refOdm (setq _refOdm __refOdm)) _refOdm)
  (:rpm_L
   (&optional __rpm_L)
   (if __rpm_L (setq _rpm_L __rpm_L)) _rpm_L)
  (:rpm_R
   (&optional __rpm_R)
   (if __rpm_R (setq _rpm_R __rpm_R)) _rpm_R)
  (:rpm_L_tmp
   (&optional __rpm_L_tmp)
   (if __rpm_L_tmp (setq _rpm_L_tmp __rpm_L_tmp)) _rpm_L_tmp)
  (:rpm_R_tmp
   (&optional __rpm_R_tmp)
   (if __rpm_R_tmp (setq _rpm_R_tmp __rpm_R_tmp)) _rpm_R_tmp)
  (:dis_L_tmp
   (&optional __dis_L_tmp)
   (if __dis_L_tmp (setq _dis_L_tmp __dis_L_tmp)) _dis_L_tmp)
  (:dis_R_tmp
   (&optional __dis_R_tmp)
   (if __dis_R_tmp (setq _dis_R_tmp __dis_R_tmp)) _dis_R_tmp)
  (:odm_L
   (&optional __odm_L)
   (if __odm_L (setq _odm_L __odm_L)) _odm_L)
  (:odm_R
   (&optional __odm_R)
   (if __odm_R (setq _odm_R __odm_R)) _odm_R)
  (:locating
   (&optional __locating)
   (if __locating (setq _locating __locating)) _locating)
  (:vel_L
   (&optional __vel_L)
   (if __vel_L (setq _vel_L __vel_L)) _vel_L)
  (:vel_R
   (&optional __vel_R)
   (if __vel_R (setq _vel_R __vel_R)) _vel_R)
  (:vel_L_tmp
   (&optional __vel_L_tmp)
   (if __vel_L_tmp (setq _vel_L_tmp __vel_L_tmp)) _vel_L_tmp)
  (:vel_R_tmp
   (&optional __vel_R_tmp)
   (if __vel_R_tmp (setq _vel_R_tmp __vel_R_tmp)) _vel_R_tmp)
  (:switch_count_pulse_L
   (&optional __switch_count_pulse_L)
   (if __switch_count_pulse_L (setq _switch_count_pulse_L __switch_count_pulse_L)) _switch_count_pulse_L)
  (:switch_count_pulse_R
   (&optional __switch_count_pulse_R)
   (if __switch_count_pulse_R (setq _switch_count_pulse_R __switch_count_pulse_R)) _switch_count_pulse_R)
  (:LrefV
   (&optional __LrefV)
   (if __LrefV (setq _LrefV __LrefV)) _LrefV)
  (:RrefV
   (&optional __RrefV)
   (if __RrefV (setq _RrefV __RrefV)) _RrefV)
  (:LerrV
   (&optional __LerrV)
   (if __LerrV (setq _LerrV __LerrV)) _LerrV)
  (:RerrV
   (&optional __RerrV)
   (if __RerrV (setq _RerrV __RerrV)) _RerrV)
  (:LcontrolV
   (&optional __LcontrolV)
   (if __LcontrolV (setq _LcontrolV __LcontrolV)) _LcontrolV)
  (:RcontrolV
   (&optional __RcontrolV)
   (if __RcontrolV (setq _RcontrolV __RcontrolV)) _RcontrolV)
  (:total_ctrl_L
   (&optional __total_ctrl_L)
   (if __total_ctrl_L (setq _total_ctrl_L __total_ctrl_L)) _total_ctrl_L)
  (:total_ctrl_R
   (&optional __total_ctrl_R)
   (if __total_ctrl_R (setq _total_ctrl_R __total_ctrl_R)) _total_ctrl_R)
  (:LRrefB
   (&optional __LRrefB)
   (if __LRrefB (setq _LRrefB __LRrefB)) _LRrefB)
  (:LRerrB
   (&optional __LRerrB)
   (if __LRerrB (setq _LRerrB __LRerrB)) _LRerrB)
  (:LcontrolB
   (&optional __LcontrolB)
   (if __LcontrolB (setq _LcontrolB __LcontrolB)) _LcontrolB)
  (:RcontrolB
   (&optional __RcontrolB)
   (if __RcontrolB (setq _RcontrolB __RcontrolB)) _RcontrolB)
  (:serialization-length
   ()
   (+
    ;; float64 _time
    8
    ;; float32 _angle_acc
    4
    ;; float32 _angle_gyro
    4
    ;; float32 _angle_filter
    4
    ;; float32 _scnt
    4
    ;; float32 _swave
    4
    ;; int16 _pwmVal
    2
    ;; float32 _refOdm
    4
    ;; int16 _rpm_L
    2
    ;; int16 _rpm_R
    2
    ;; int16 _rpm_L_tmp
    2
    ;; int16 _rpm_R_tmp
    2
    ;; int32 _dis_L_tmp
    4
    ;; int32 _dis_R_tmp
    4
    ;; float32 _odm_L
    4
    ;; float32 _odm_R
    4
    ;; bool _locating
    1
    ;; float32 _vel_L
    4
    ;; float32 _vel_R
    4
    ;; int32 _vel_L_tmp
    4
    ;; int32 _vel_R_tmp
    4
    ;; int16 _switch_count_pulse_L
    2
    ;; int16 _switch_count_pulse_R
    2
    ;; int16 _LrefV
    2
    ;; int16 _RrefV
    2
    ;; float32 _LerrV
    4
    ;; float32 _RerrV
    4
    ;; float32 _LcontrolV
    4
    ;; float32 _RcontrolV
    4
    ;; float32 _total_ctrl_L
    4
    ;; float32 _total_ctrl_R
    4
    ;; float32 _LRrefB
    4
    ;; float32 _LRerrB
    4
    ;; float32 _LcontrolB
    4
    ;; float32 _RcontrolB
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _time
       (sys::poke _time (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float32 _angle_acc
       (sys::poke _angle_acc (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _angle_gyro
       (sys::poke _angle_gyro (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _angle_filter
       (sys::poke _angle_filter (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _scnt
       (sys::poke _scnt (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _swave
       (sys::poke _swave (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int16 _pwmVal
       (write-word _pwmVal s)
     ;; float32 _refOdm
       (sys::poke _refOdm (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int16 _rpm_L
       (write-word _rpm_L s)
     ;; int16 _rpm_R
       (write-word _rpm_R s)
     ;; int16 _rpm_L_tmp
       (write-word _rpm_L_tmp s)
     ;; int16 _rpm_R_tmp
       (write-word _rpm_R_tmp s)
     ;; int32 _dis_L_tmp
       (write-long _dis_L_tmp s)
     ;; int32 _dis_R_tmp
       (write-long _dis_R_tmp s)
     ;; float32 _odm_L
       (sys::poke _odm_L (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _odm_R
       (sys::poke _odm_R (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _locating
       (if _locating (write-byte -1 s) (write-byte 0 s))
     ;; float32 _vel_L
       (sys::poke _vel_L (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _vel_R
       (sys::poke _vel_R (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; int32 _vel_L_tmp
       (write-long _vel_L_tmp s)
     ;; int32 _vel_R_tmp
       (write-long _vel_R_tmp s)
     ;; int16 _switch_count_pulse_L
       (write-word _switch_count_pulse_L s)
     ;; int16 _switch_count_pulse_R
       (write-word _switch_count_pulse_R s)
     ;; int16 _LrefV
       (write-word _LrefV s)
     ;; int16 _RrefV
       (write-word _RrefV s)
     ;; float32 _LerrV
       (sys::poke _LerrV (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _RerrV
       (sys::poke _RerrV (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _LcontrolV
       (sys::poke _LcontrolV (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _RcontrolV
       (sys::poke _RcontrolV (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _total_ctrl_L
       (sys::poke _total_ctrl_L (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _total_ctrl_R
       (sys::poke _total_ctrl_R (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _LRrefB
       (sys::poke _LRrefB (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _LRerrB
       (sys::poke _LRerrB (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _LcontrolB
       (sys::poke _LcontrolB (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _RcontrolB
       (sys::poke _RcontrolB (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _time
     (setq _time (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float32 _angle_acc
     (setq _angle_acc (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _angle_gyro
     (setq _angle_gyro (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _angle_filter
     (setq _angle_filter (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _scnt
     (setq _scnt (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _swave
     (setq _swave (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int16 _pwmVal
     (setq _pwmVal (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; float32 _refOdm
     (setq _refOdm (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int16 _rpm_L
     (setq _rpm_L (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _rpm_R
     (setq _rpm_R (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _rpm_L_tmp
     (setq _rpm_L_tmp (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _rpm_R_tmp
     (setq _rpm_R_tmp (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int32 _dis_L_tmp
     (setq _dis_L_tmp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _dis_R_tmp
     (setq _dis_R_tmp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float32 _odm_L
     (setq _odm_L (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _odm_R
     (setq _odm_R (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _locating
     (setq _locating (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _vel_L
     (setq _vel_L (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _vel_R
     (setq _vel_R (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; int32 _vel_L_tmp
     (setq _vel_L_tmp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _vel_R_tmp
     (setq _vel_R_tmp (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int16 _switch_count_pulse_L
     (setq _switch_count_pulse_L (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _switch_count_pulse_R
     (setq _switch_count_pulse_R (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _LrefV
     (setq _LrefV (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _RrefV
     (setq _RrefV (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; float32 _LerrV
     (setq _LerrV (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _RerrV
     (setq _RerrV (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _LcontrolV
     (setq _LcontrolV (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _RcontrolV
     (setq _RcontrolV (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _total_ctrl_L
     (setq _total_ctrl_L (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _total_ctrl_R
     (setq _total_ctrl_R (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _LRrefB
     (setq _LRrefB (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _LRerrB
     (setq _LRerrB (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _LcontrolB
     (setq _LcontrolB (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _RcontrolB
     (setq _RcontrolB (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get custom_msgs::pendulum_status :md5sum-) "0901b4abd021ac391383299961900100")
(setf (get custom_msgs::pendulum_status :datatype-) "custom_msgs/pendulum_status")
(setf (get custom_msgs::pendulum_status :definition-)
      "float64 time

float32 angle_acc
float32 angle_gyro
float32 angle_filter

float32 scnt
float32 swave
int16 pwmVal
float32 refOdm

int16 rpm_L
int16 rpm_R
int16 rpm_L_tmp
int16 rpm_R_tmp
int32 dis_L_tmp
int32 dis_R_tmp
float32 odm_L
float32 odm_R
bool locating
float32 vel_L
float32 vel_R
int32 vel_L_tmp
int32 vel_R_tmp
int16 switch_count_pulse_L
int16 switch_count_pulse_R

int16 LrefV
int16 RrefV
float32 LerrV
float32 RerrV
float32 LcontrolV
float32 RcontrolV
float32 total_ctrl_L
float32 total_ctrl_R

float32 LRrefB
float32 LRerrB
float32 LcontrolB
float32 RcontrolB



")



(provide :custom_msgs/pendulum_status "0901b4abd021ac391383299961900100")


