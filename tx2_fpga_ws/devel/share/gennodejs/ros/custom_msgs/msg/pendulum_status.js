// Auto-generated. Do not edit!

// (in-package custom_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class pendulum_status {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.time = null;
      this.angle_acc = null;
      this.angle_gyro = null;
      this.angle_filter = null;
      this.scnt = null;
      this.swave = null;
      this.pwmVal = null;
      this.refOdm = null;
      this.rpm_L = null;
      this.rpm_R = null;
      this.rpm_L_tmp = null;
      this.rpm_R_tmp = null;
      this.dis_L_tmp = null;
      this.dis_R_tmp = null;
      this.odm_L = null;
      this.odm_R = null;
      this.locating = null;
      this.vel_L = null;
      this.vel_R = null;
      this.vel_L_tmp = null;
      this.vel_R_tmp = null;
      this.switch_count_pulse_L = null;
      this.switch_count_pulse_R = null;
      this.LrefV = null;
      this.RrefV = null;
      this.LerrV = null;
      this.RerrV = null;
      this.LcontrolV = null;
      this.RcontrolV = null;
      this.total_ctrl_L = null;
      this.total_ctrl_R = null;
      this.LRrefB = null;
      this.LRerrB = null;
      this.LcontrolB = null;
      this.RcontrolB = null;
    }
    else {
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = 0.0;
      }
      if (initObj.hasOwnProperty('angle_acc')) {
        this.angle_acc = initObj.angle_acc
      }
      else {
        this.angle_acc = 0.0;
      }
      if (initObj.hasOwnProperty('angle_gyro')) {
        this.angle_gyro = initObj.angle_gyro
      }
      else {
        this.angle_gyro = 0.0;
      }
      if (initObj.hasOwnProperty('angle_filter')) {
        this.angle_filter = initObj.angle_filter
      }
      else {
        this.angle_filter = 0.0;
      }
      if (initObj.hasOwnProperty('scnt')) {
        this.scnt = initObj.scnt
      }
      else {
        this.scnt = 0.0;
      }
      if (initObj.hasOwnProperty('swave')) {
        this.swave = initObj.swave
      }
      else {
        this.swave = 0.0;
      }
      if (initObj.hasOwnProperty('pwmVal')) {
        this.pwmVal = initObj.pwmVal
      }
      else {
        this.pwmVal = 0;
      }
      if (initObj.hasOwnProperty('refOdm')) {
        this.refOdm = initObj.refOdm
      }
      else {
        this.refOdm = 0.0;
      }
      if (initObj.hasOwnProperty('rpm_L')) {
        this.rpm_L = initObj.rpm_L
      }
      else {
        this.rpm_L = 0;
      }
      if (initObj.hasOwnProperty('rpm_R')) {
        this.rpm_R = initObj.rpm_R
      }
      else {
        this.rpm_R = 0;
      }
      if (initObj.hasOwnProperty('rpm_L_tmp')) {
        this.rpm_L_tmp = initObj.rpm_L_tmp
      }
      else {
        this.rpm_L_tmp = 0;
      }
      if (initObj.hasOwnProperty('rpm_R_tmp')) {
        this.rpm_R_tmp = initObj.rpm_R_tmp
      }
      else {
        this.rpm_R_tmp = 0;
      }
      if (initObj.hasOwnProperty('dis_L_tmp')) {
        this.dis_L_tmp = initObj.dis_L_tmp
      }
      else {
        this.dis_L_tmp = 0;
      }
      if (initObj.hasOwnProperty('dis_R_tmp')) {
        this.dis_R_tmp = initObj.dis_R_tmp
      }
      else {
        this.dis_R_tmp = 0;
      }
      if (initObj.hasOwnProperty('odm_L')) {
        this.odm_L = initObj.odm_L
      }
      else {
        this.odm_L = 0.0;
      }
      if (initObj.hasOwnProperty('odm_R')) {
        this.odm_R = initObj.odm_R
      }
      else {
        this.odm_R = 0.0;
      }
      if (initObj.hasOwnProperty('locating')) {
        this.locating = initObj.locating
      }
      else {
        this.locating = false;
      }
      if (initObj.hasOwnProperty('vel_L')) {
        this.vel_L = initObj.vel_L
      }
      else {
        this.vel_L = 0.0;
      }
      if (initObj.hasOwnProperty('vel_R')) {
        this.vel_R = initObj.vel_R
      }
      else {
        this.vel_R = 0.0;
      }
      if (initObj.hasOwnProperty('vel_L_tmp')) {
        this.vel_L_tmp = initObj.vel_L_tmp
      }
      else {
        this.vel_L_tmp = 0;
      }
      if (initObj.hasOwnProperty('vel_R_tmp')) {
        this.vel_R_tmp = initObj.vel_R_tmp
      }
      else {
        this.vel_R_tmp = 0;
      }
      if (initObj.hasOwnProperty('switch_count_pulse_L')) {
        this.switch_count_pulse_L = initObj.switch_count_pulse_L
      }
      else {
        this.switch_count_pulse_L = 0;
      }
      if (initObj.hasOwnProperty('switch_count_pulse_R')) {
        this.switch_count_pulse_R = initObj.switch_count_pulse_R
      }
      else {
        this.switch_count_pulse_R = 0;
      }
      if (initObj.hasOwnProperty('LrefV')) {
        this.LrefV = initObj.LrefV
      }
      else {
        this.LrefV = 0;
      }
      if (initObj.hasOwnProperty('RrefV')) {
        this.RrefV = initObj.RrefV
      }
      else {
        this.RrefV = 0;
      }
      if (initObj.hasOwnProperty('LerrV')) {
        this.LerrV = initObj.LerrV
      }
      else {
        this.LerrV = 0.0;
      }
      if (initObj.hasOwnProperty('RerrV')) {
        this.RerrV = initObj.RerrV
      }
      else {
        this.RerrV = 0.0;
      }
      if (initObj.hasOwnProperty('LcontrolV')) {
        this.LcontrolV = initObj.LcontrolV
      }
      else {
        this.LcontrolV = 0.0;
      }
      if (initObj.hasOwnProperty('RcontrolV')) {
        this.RcontrolV = initObj.RcontrolV
      }
      else {
        this.RcontrolV = 0.0;
      }
      if (initObj.hasOwnProperty('total_ctrl_L')) {
        this.total_ctrl_L = initObj.total_ctrl_L
      }
      else {
        this.total_ctrl_L = 0.0;
      }
      if (initObj.hasOwnProperty('total_ctrl_R')) {
        this.total_ctrl_R = initObj.total_ctrl_R
      }
      else {
        this.total_ctrl_R = 0.0;
      }
      if (initObj.hasOwnProperty('LRrefB')) {
        this.LRrefB = initObj.LRrefB
      }
      else {
        this.LRrefB = 0.0;
      }
      if (initObj.hasOwnProperty('LRerrB')) {
        this.LRerrB = initObj.LRerrB
      }
      else {
        this.LRerrB = 0.0;
      }
      if (initObj.hasOwnProperty('LcontrolB')) {
        this.LcontrolB = initObj.LcontrolB
      }
      else {
        this.LcontrolB = 0.0;
      }
      if (initObj.hasOwnProperty('RcontrolB')) {
        this.RcontrolB = initObj.RcontrolB
      }
      else {
        this.RcontrolB = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type pendulum_status
    // Serialize message field [time]
    bufferOffset = _serializer.float64(obj.time, buffer, bufferOffset);
    // Serialize message field [angle_acc]
    bufferOffset = _serializer.float32(obj.angle_acc, buffer, bufferOffset);
    // Serialize message field [angle_gyro]
    bufferOffset = _serializer.float32(obj.angle_gyro, buffer, bufferOffset);
    // Serialize message field [angle_filter]
    bufferOffset = _serializer.float32(obj.angle_filter, buffer, bufferOffset);
    // Serialize message field [scnt]
    bufferOffset = _serializer.float32(obj.scnt, buffer, bufferOffset);
    // Serialize message field [swave]
    bufferOffset = _serializer.float32(obj.swave, buffer, bufferOffset);
    // Serialize message field [pwmVal]
    bufferOffset = _serializer.int16(obj.pwmVal, buffer, bufferOffset);
    // Serialize message field [refOdm]
    bufferOffset = _serializer.float32(obj.refOdm, buffer, bufferOffset);
    // Serialize message field [rpm_L]
    bufferOffset = _serializer.int16(obj.rpm_L, buffer, bufferOffset);
    // Serialize message field [rpm_R]
    bufferOffset = _serializer.int16(obj.rpm_R, buffer, bufferOffset);
    // Serialize message field [rpm_L_tmp]
    bufferOffset = _serializer.int16(obj.rpm_L_tmp, buffer, bufferOffset);
    // Serialize message field [rpm_R_tmp]
    bufferOffset = _serializer.int16(obj.rpm_R_tmp, buffer, bufferOffset);
    // Serialize message field [dis_L_tmp]
    bufferOffset = _serializer.int32(obj.dis_L_tmp, buffer, bufferOffset);
    // Serialize message field [dis_R_tmp]
    bufferOffset = _serializer.int32(obj.dis_R_tmp, buffer, bufferOffset);
    // Serialize message field [odm_L]
    bufferOffset = _serializer.float32(obj.odm_L, buffer, bufferOffset);
    // Serialize message field [odm_R]
    bufferOffset = _serializer.float32(obj.odm_R, buffer, bufferOffset);
    // Serialize message field [locating]
    bufferOffset = _serializer.bool(obj.locating, buffer, bufferOffset);
    // Serialize message field [vel_L]
    bufferOffset = _serializer.float32(obj.vel_L, buffer, bufferOffset);
    // Serialize message field [vel_R]
    bufferOffset = _serializer.float32(obj.vel_R, buffer, bufferOffset);
    // Serialize message field [vel_L_tmp]
    bufferOffset = _serializer.int32(obj.vel_L_tmp, buffer, bufferOffset);
    // Serialize message field [vel_R_tmp]
    bufferOffset = _serializer.int32(obj.vel_R_tmp, buffer, bufferOffset);
    // Serialize message field [switch_count_pulse_L]
    bufferOffset = _serializer.int16(obj.switch_count_pulse_L, buffer, bufferOffset);
    // Serialize message field [switch_count_pulse_R]
    bufferOffset = _serializer.int16(obj.switch_count_pulse_R, buffer, bufferOffset);
    // Serialize message field [LrefV]
    bufferOffset = _serializer.int16(obj.LrefV, buffer, bufferOffset);
    // Serialize message field [RrefV]
    bufferOffset = _serializer.int16(obj.RrefV, buffer, bufferOffset);
    // Serialize message field [LerrV]
    bufferOffset = _serializer.float32(obj.LerrV, buffer, bufferOffset);
    // Serialize message field [RerrV]
    bufferOffset = _serializer.float32(obj.RerrV, buffer, bufferOffset);
    // Serialize message field [LcontrolV]
    bufferOffset = _serializer.float32(obj.LcontrolV, buffer, bufferOffset);
    // Serialize message field [RcontrolV]
    bufferOffset = _serializer.float32(obj.RcontrolV, buffer, bufferOffset);
    // Serialize message field [total_ctrl_L]
    bufferOffset = _serializer.float32(obj.total_ctrl_L, buffer, bufferOffset);
    // Serialize message field [total_ctrl_R]
    bufferOffset = _serializer.float32(obj.total_ctrl_R, buffer, bufferOffset);
    // Serialize message field [LRrefB]
    bufferOffset = _serializer.float32(obj.LRrefB, buffer, bufferOffset);
    // Serialize message field [LRerrB]
    bufferOffset = _serializer.float32(obj.LRerrB, buffer, bufferOffset);
    // Serialize message field [LcontrolB]
    bufferOffset = _serializer.float32(obj.LcontrolB, buffer, bufferOffset);
    // Serialize message field [RcontrolB]
    bufferOffset = _serializer.float32(obj.RcontrolB, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type pendulum_status
    let len;
    let data = new pendulum_status(null);
    // Deserialize message field [time]
    data.time = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [angle_acc]
    data.angle_acc = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [angle_gyro]
    data.angle_gyro = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [angle_filter]
    data.angle_filter = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [scnt]
    data.scnt = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [swave]
    data.swave = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pwmVal]
    data.pwmVal = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [refOdm]
    data.refOdm = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [rpm_L]
    data.rpm_L = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [rpm_R]
    data.rpm_R = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [rpm_L_tmp]
    data.rpm_L_tmp = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [rpm_R_tmp]
    data.rpm_R_tmp = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [dis_L_tmp]
    data.dis_L_tmp = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [dis_R_tmp]
    data.dis_R_tmp = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [odm_L]
    data.odm_L = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [odm_R]
    data.odm_R = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [locating]
    data.locating = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [vel_L]
    data.vel_L = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [vel_R]
    data.vel_R = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [vel_L_tmp]
    data.vel_L_tmp = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [vel_R_tmp]
    data.vel_R_tmp = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [switch_count_pulse_L]
    data.switch_count_pulse_L = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [switch_count_pulse_R]
    data.switch_count_pulse_R = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [LrefV]
    data.LrefV = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [RrefV]
    data.RrefV = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [LerrV]
    data.LerrV = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [RerrV]
    data.RerrV = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [LcontrolV]
    data.LcontrolV = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [RcontrolV]
    data.RcontrolV = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [total_ctrl_L]
    data.total_ctrl_L = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [total_ctrl_R]
    data.total_ctrl_R = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [LRrefB]
    data.LRrefB = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [LRerrB]
    data.LRerrB = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [LcontrolB]
    data.LcontrolB = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [RcontrolB]
    data.RcontrolB = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 123;
  }

  static datatype() {
    // Returns string type for a message object
    return 'custom_msgs/pendulum_status';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '0901b4abd021ac391383299961900100';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 time
    
    float32 angle_acc
    float32 angle_gyro
    float32 angle_filter
    
    float32 scnt
    float32 swave
    int16 pwmVal
    float32 refOdm
    
    int16 rpm_L
    int16 rpm_R
    int16 rpm_L_tmp
    int16 rpm_R_tmp
    int32 dis_L_tmp
    int32 dis_R_tmp
    float32 odm_L
    float32 odm_R
    bool locating
    float32 vel_L
    float32 vel_R
    int32 vel_L_tmp
    int32 vel_R_tmp
    int16 switch_count_pulse_L
    int16 switch_count_pulse_R
    
    int16 LrefV
    int16 RrefV
    float32 LerrV
    float32 RerrV
    float32 LcontrolV
    float32 RcontrolV
    float32 total_ctrl_L
    float32 total_ctrl_R
    
    float32 LRrefB
    float32 LRerrB
    float32 LcontrolB
    float32 RcontrolB
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new pendulum_status(null);
    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = 0.0
    }

    if (msg.angle_acc !== undefined) {
      resolved.angle_acc = msg.angle_acc;
    }
    else {
      resolved.angle_acc = 0.0
    }

    if (msg.angle_gyro !== undefined) {
      resolved.angle_gyro = msg.angle_gyro;
    }
    else {
      resolved.angle_gyro = 0.0
    }

    if (msg.angle_filter !== undefined) {
      resolved.angle_filter = msg.angle_filter;
    }
    else {
      resolved.angle_filter = 0.0
    }

    if (msg.scnt !== undefined) {
      resolved.scnt = msg.scnt;
    }
    else {
      resolved.scnt = 0.0
    }

    if (msg.swave !== undefined) {
      resolved.swave = msg.swave;
    }
    else {
      resolved.swave = 0.0
    }

    if (msg.pwmVal !== undefined) {
      resolved.pwmVal = msg.pwmVal;
    }
    else {
      resolved.pwmVal = 0
    }

    if (msg.refOdm !== undefined) {
      resolved.refOdm = msg.refOdm;
    }
    else {
      resolved.refOdm = 0.0
    }

    if (msg.rpm_L !== undefined) {
      resolved.rpm_L = msg.rpm_L;
    }
    else {
      resolved.rpm_L = 0
    }

    if (msg.rpm_R !== undefined) {
      resolved.rpm_R = msg.rpm_R;
    }
    else {
      resolved.rpm_R = 0
    }

    if (msg.rpm_L_tmp !== undefined) {
      resolved.rpm_L_tmp = msg.rpm_L_tmp;
    }
    else {
      resolved.rpm_L_tmp = 0
    }

    if (msg.rpm_R_tmp !== undefined) {
      resolved.rpm_R_tmp = msg.rpm_R_tmp;
    }
    else {
      resolved.rpm_R_tmp = 0
    }

    if (msg.dis_L_tmp !== undefined) {
      resolved.dis_L_tmp = msg.dis_L_tmp;
    }
    else {
      resolved.dis_L_tmp = 0
    }

    if (msg.dis_R_tmp !== undefined) {
      resolved.dis_R_tmp = msg.dis_R_tmp;
    }
    else {
      resolved.dis_R_tmp = 0
    }

    if (msg.odm_L !== undefined) {
      resolved.odm_L = msg.odm_L;
    }
    else {
      resolved.odm_L = 0.0
    }

    if (msg.odm_R !== undefined) {
      resolved.odm_R = msg.odm_R;
    }
    else {
      resolved.odm_R = 0.0
    }

    if (msg.locating !== undefined) {
      resolved.locating = msg.locating;
    }
    else {
      resolved.locating = false
    }

    if (msg.vel_L !== undefined) {
      resolved.vel_L = msg.vel_L;
    }
    else {
      resolved.vel_L = 0.0
    }

    if (msg.vel_R !== undefined) {
      resolved.vel_R = msg.vel_R;
    }
    else {
      resolved.vel_R = 0.0
    }

    if (msg.vel_L_tmp !== undefined) {
      resolved.vel_L_tmp = msg.vel_L_tmp;
    }
    else {
      resolved.vel_L_tmp = 0
    }

    if (msg.vel_R_tmp !== undefined) {
      resolved.vel_R_tmp = msg.vel_R_tmp;
    }
    else {
      resolved.vel_R_tmp = 0
    }

    if (msg.switch_count_pulse_L !== undefined) {
      resolved.switch_count_pulse_L = msg.switch_count_pulse_L;
    }
    else {
      resolved.switch_count_pulse_L = 0
    }

    if (msg.switch_count_pulse_R !== undefined) {
      resolved.switch_count_pulse_R = msg.switch_count_pulse_R;
    }
    else {
      resolved.switch_count_pulse_R = 0
    }

    if (msg.LrefV !== undefined) {
      resolved.LrefV = msg.LrefV;
    }
    else {
      resolved.LrefV = 0
    }

    if (msg.RrefV !== undefined) {
      resolved.RrefV = msg.RrefV;
    }
    else {
      resolved.RrefV = 0
    }

    if (msg.LerrV !== undefined) {
      resolved.LerrV = msg.LerrV;
    }
    else {
      resolved.LerrV = 0.0
    }

    if (msg.RerrV !== undefined) {
      resolved.RerrV = msg.RerrV;
    }
    else {
      resolved.RerrV = 0.0
    }

    if (msg.LcontrolV !== undefined) {
      resolved.LcontrolV = msg.LcontrolV;
    }
    else {
      resolved.LcontrolV = 0.0
    }

    if (msg.RcontrolV !== undefined) {
      resolved.RcontrolV = msg.RcontrolV;
    }
    else {
      resolved.RcontrolV = 0.0
    }

    if (msg.total_ctrl_L !== undefined) {
      resolved.total_ctrl_L = msg.total_ctrl_L;
    }
    else {
      resolved.total_ctrl_L = 0.0
    }

    if (msg.total_ctrl_R !== undefined) {
      resolved.total_ctrl_R = msg.total_ctrl_R;
    }
    else {
      resolved.total_ctrl_R = 0.0
    }

    if (msg.LRrefB !== undefined) {
      resolved.LRrefB = msg.LRrefB;
    }
    else {
      resolved.LRrefB = 0.0
    }

    if (msg.LRerrB !== undefined) {
      resolved.LRerrB = msg.LRerrB;
    }
    else {
      resolved.LRerrB = 0.0
    }

    if (msg.LcontrolB !== undefined) {
      resolved.LcontrolB = msg.LcontrolB;
    }
    else {
      resolved.LcontrolB = 0.0
    }

    if (msg.RcontrolB !== undefined) {
      resolved.RcontrolB = msg.RcontrolB;
    }
    else {
      resolved.RcontrolB = 0.0
    }

    return resolved;
    }
};

module.exports = pendulum_status;
