// Auto-generated. Do not edit!

// (in-package custom_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class pwmVal_refRPM {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.pwmVal = null;
      this.refRPM = null;
    }
    else {
      if (initObj.hasOwnProperty('pwmVal')) {
        this.pwmVal = initObj.pwmVal
      }
      else {
        this.pwmVal = 0;
      }
      if (initObj.hasOwnProperty('refRPM')) {
        this.refRPM = initObj.refRPM
      }
      else {
        this.refRPM = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type pwmVal_refRPM
    // Serialize message field [pwmVal]
    bufferOffset = _serializer.int16(obj.pwmVal, buffer, bufferOffset);
    // Serialize message field [refRPM]
    bufferOffset = _serializer.int16(obj.refRPM, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type pwmVal_refRPM
    let len;
    let data = new pwmVal_refRPM(null);
    // Deserialize message field [pwmVal]
    data.pwmVal = _deserializer.int16(buffer, bufferOffset);
    // Deserialize message field [refRPM]
    data.refRPM = _deserializer.int16(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'custom_msgs/pwmVal_refRPM';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a65cdb1254a070de0d815cb99d141f87';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int16 pwmVal
    int16 refRPM
    
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new pwmVal_refRPM(null);
    if (msg.pwmVal !== undefined) {
      resolved.pwmVal = msg.pwmVal;
    }
    else {
      resolved.pwmVal = 0
    }

    if (msg.refRPM !== undefined) {
      resolved.refRPM = msg.refRPM;
    }
    else {
      resolved.refRPM = 0
    }

    return resolved;
    }
};

module.exports = pwmVal_refRPM;
