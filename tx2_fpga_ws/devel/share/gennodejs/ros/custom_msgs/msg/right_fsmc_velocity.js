// Auto-generated. Do not edit!

// (in-package custom_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class right_fsmc_velocity {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.lamda = null;
      this.gs1 = null;
      this.gs2 = null;
      this.gs3 = null;
      this.gu1 = null;
      this.gu2 = null;
      this.gu3 = null;
      this.controlMax = null;
    }
    else {
      if (initObj.hasOwnProperty('lamda')) {
        this.lamda = initObj.lamda
      }
      else {
        this.lamda = 0.0;
      }
      if (initObj.hasOwnProperty('gs1')) {
        this.gs1 = initObj.gs1
      }
      else {
        this.gs1 = 0.0;
      }
      if (initObj.hasOwnProperty('gs2')) {
        this.gs2 = initObj.gs2
      }
      else {
        this.gs2 = 0.0;
      }
      if (initObj.hasOwnProperty('gs3')) {
        this.gs3 = initObj.gs3
      }
      else {
        this.gs3 = 0.0;
      }
      if (initObj.hasOwnProperty('gu1')) {
        this.gu1 = initObj.gu1
      }
      else {
        this.gu1 = 0.0;
      }
      if (initObj.hasOwnProperty('gu2')) {
        this.gu2 = initObj.gu2
      }
      else {
        this.gu2 = 0.0;
      }
      if (initObj.hasOwnProperty('gu3')) {
        this.gu3 = initObj.gu3
      }
      else {
        this.gu3 = 0.0;
      }
      if (initObj.hasOwnProperty('controlMax')) {
        this.controlMax = initObj.controlMax
      }
      else {
        this.controlMax = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type right_fsmc_velocity
    // Serialize message field [lamda]
    bufferOffset = _serializer.float32(obj.lamda, buffer, bufferOffset);
    // Serialize message field [gs1]
    bufferOffset = _serializer.float32(obj.gs1, buffer, bufferOffset);
    // Serialize message field [gs2]
    bufferOffset = _serializer.float32(obj.gs2, buffer, bufferOffset);
    // Serialize message field [gs3]
    bufferOffset = _serializer.float32(obj.gs3, buffer, bufferOffset);
    // Serialize message field [gu1]
    bufferOffset = _serializer.float32(obj.gu1, buffer, bufferOffset);
    // Serialize message field [gu2]
    bufferOffset = _serializer.float32(obj.gu2, buffer, bufferOffset);
    // Serialize message field [gu3]
    bufferOffset = _serializer.float32(obj.gu3, buffer, bufferOffset);
    // Serialize message field [controlMax]
    bufferOffset = _serializer.float32(obj.controlMax, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type right_fsmc_velocity
    let len;
    let data = new right_fsmc_velocity(null);
    // Deserialize message field [lamda]
    data.lamda = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gs1]
    data.gs1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gs2]
    data.gs2 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gs3]
    data.gs3 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gu1]
    data.gu1 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gu2]
    data.gu2 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [gu3]
    data.gu3 = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [controlMax]
    data.controlMax = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 32;
  }

  static datatype() {
    // Returns string type for a message object
    return 'custom_msgs/right_fsmc_velocity';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'dcef653360ac32e30e928872b8672b09';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 lamda
    float32 gs1
    float32 gs2
    float32 gs3
    float32 gu1
    float32 gu2
    float32 gu3
    float32 controlMax
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new right_fsmc_velocity(null);
    if (msg.lamda !== undefined) {
      resolved.lamda = msg.lamda;
    }
    else {
      resolved.lamda = 0.0
    }

    if (msg.gs1 !== undefined) {
      resolved.gs1 = msg.gs1;
    }
    else {
      resolved.gs1 = 0.0
    }

    if (msg.gs2 !== undefined) {
      resolved.gs2 = msg.gs2;
    }
    else {
      resolved.gs2 = 0.0
    }

    if (msg.gs3 !== undefined) {
      resolved.gs3 = msg.gs3;
    }
    else {
      resolved.gs3 = 0.0
    }

    if (msg.gu1 !== undefined) {
      resolved.gu1 = msg.gu1;
    }
    else {
      resolved.gu1 = 0.0
    }

    if (msg.gu2 !== undefined) {
      resolved.gu2 = msg.gu2;
    }
    else {
      resolved.gu2 = 0.0
    }

    if (msg.gu3 !== undefined) {
      resolved.gu3 = msg.gu3;
    }
    else {
      resolved.gu3 = 0.0
    }

    if (msg.controlMax !== undefined) {
      resolved.controlMax = msg.controlMax;
    }
    else {
      resolved.controlMax = 0.0
    }

    return resolved;
    }
};

module.exports = right_fsmc_velocity;
