; Auto-generated. Do not edit!


(cl:in-package custom_msgs-msg)


;//! \htmlinclude pwmVal_refRPM.msg.html

(cl:defclass <pwmVal_refRPM> (roslisp-msg-protocol:ros-message)
  ((pwmVal
    :reader pwmVal
    :initarg :pwmVal
    :type cl:fixnum
    :initform 0)
   (refRPM
    :reader refRPM
    :initarg :refRPM
    :type cl:fixnum
    :initform 0))
)

(cl:defclass pwmVal_refRPM (<pwmVal_refRPM>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <pwmVal_refRPM>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'pwmVal_refRPM)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name custom_msgs-msg:<pwmVal_refRPM> is deprecated: use custom_msgs-msg:pwmVal_refRPM instead.")))

(cl:ensure-generic-function 'pwmVal-val :lambda-list '(m))
(cl:defmethod pwmVal-val ((m <pwmVal_refRPM>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:pwmVal-val is deprecated.  Use custom_msgs-msg:pwmVal instead.")
  (pwmVal m))

(cl:ensure-generic-function 'refRPM-val :lambda-list '(m))
(cl:defmethod refRPM-val ((m <pwmVal_refRPM>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:refRPM-val is deprecated.  Use custom_msgs-msg:refRPM instead.")
  (refRPM m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <pwmVal_refRPM>) ostream)
  "Serializes a message object of type '<pwmVal_refRPM>"
  (cl:let* ((signed (cl:slot-value msg 'pwmVal)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'refRPM)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <pwmVal_refRPM>) istream)
  "Deserializes a message object of type '<pwmVal_refRPM>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'pwmVal) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'refRPM) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<pwmVal_refRPM>)))
  "Returns string type for a message object of type '<pwmVal_refRPM>"
  "custom_msgs/pwmVal_refRPM")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'pwmVal_refRPM)))
  "Returns string type for a message object of type 'pwmVal_refRPM"
  "custom_msgs/pwmVal_refRPM")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<pwmVal_refRPM>)))
  "Returns md5sum for a message object of type '<pwmVal_refRPM>"
  "a65cdb1254a070de0d815cb99d141f87")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'pwmVal_refRPM)))
  "Returns md5sum for a message object of type 'pwmVal_refRPM"
  "a65cdb1254a070de0d815cb99d141f87")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<pwmVal_refRPM>)))
  "Returns full string definition for message of type '<pwmVal_refRPM>"
  (cl:format cl:nil "int16 pwmVal~%int16 refRPM~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'pwmVal_refRPM)))
  "Returns full string definition for message of type 'pwmVal_refRPM"
  (cl:format cl:nil "int16 pwmVal~%int16 refRPM~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <pwmVal_refRPM>))
  (cl:+ 0
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <pwmVal_refRPM>))
  "Converts a ROS message object to a list"
  (cl:list 'pwmVal_refRPM
    (cl:cons ':pwmVal (pwmVal msg))
    (cl:cons ':refRPM (refRPM msg))
))
