(cl:in-package custom_msgs-msg)
(cl:export '(LAMDA-VAL
          LAMDA
          GS1-VAL
          GS1
          GS2-VAL
          GS2
          GS3-VAL
          GS3
          GU1-VAL
          GU1
          GU2-VAL
          GU2
          GU3-VAL
          GU3
          CONTROLMAX-VAL
          CONTROLMAX
))