; Auto-generated. Do not edit!


(cl:in-package custom_msgs-msg)


;//! \htmlinclude left_pid_velocity.msg.html

(cl:defclass <left_pid_velocity> (roslisp-msg-protocol:ros-message)
  ((kp
    :reader kp
    :initarg :kp
    :type cl:float
    :initform 0.0)
   (ki
    :reader ki
    :initarg :ki
    :type cl:float
    :initform 0.0)
   (kd
    :reader kd
    :initarg :kd
    :type cl:float
    :initform 0.0)
   (controlMax
    :reader controlMax
    :initarg :controlMax
    :type cl:float
    :initform 0.0))
)

(cl:defclass left_pid_velocity (<left_pid_velocity>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <left_pid_velocity>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'left_pid_velocity)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name custom_msgs-msg:<left_pid_velocity> is deprecated: use custom_msgs-msg:left_pid_velocity instead.")))

(cl:ensure-generic-function 'kp-val :lambda-list '(m))
(cl:defmethod kp-val ((m <left_pid_velocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:kp-val is deprecated.  Use custom_msgs-msg:kp instead.")
  (kp m))

(cl:ensure-generic-function 'ki-val :lambda-list '(m))
(cl:defmethod ki-val ((m <left_pid_velocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:ki-val is deprecated.  Use custom_msgs-msg:ki instead.")
  (ki m))

(cl:ensure-generic-function 'kd-val :lambda-list '(m))
(cl:defmethod kd-val ((m <left_pid_velocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:kd-val is deprecated.  Use custom_msgs-msg:kd instead.")
  (kd m))

(cl:ensure-generic-function 'controlMax-val :lambda-list '(m))
(cl:defmethod controlMax-val ((m <left_pid_velocity>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:controlMax-val is deprecated.  Use custom_msgs-msg:controlMax instead.")
  (controlMax m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <left_pid_velocity>) ostream)
  "Serializes a message object of type '<left_pid_velocity>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'kp))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ki))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'kd))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'controlMax))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <left_pid_velocity>) istream)
  "Deserializes a message object of type '<left_pid_velocity>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'kp) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ki) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'kd) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'controlMax) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<left_pid_velocity>)))
  "Returns string type for a message object of type '<left_pid_velocity>"
  "custom_msgs/left_pid_velocity")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'left_pid_velocity)))
  "Returns string type for a message object of type 'left_pid_velocity"
  "custom_msgs/left_pid_velocity")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<left_pid_velocity>)))
  "Returns md5sum for a message object of type '<left_pid_velocity>"
  "623d8ff803e92a174b2a9e53a920d15c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'left_pid_velocity)))
  "Returns md5sum for a message object of type 'left_pid_velocity"
  "623d8ff803e92a174b2a9e53a920d15c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<left_pid_velocity>)))
  "Returns full string definition for message of type '<left_pid_velocity>"
  (cl:format cl:nil "float32 kp~%float32 ki~%float32 kd~%float32 controlMax~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'left_pid_velocity)))
  "Returns full string definition for message of type 'left_pid_velocity"
  (cl:format cl:nil "float32 kp~%float32 ki~%float32 kd~%float32 controlMax~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <left_pid_velocity>))
  (cl:+ 0
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <left_pid_velocity>))
  "Converts a ROS message object to a list"
  (cl:list 'left_pid_velocity
    (cl:cons ':kp (kp msg))
    (cl:cons ':ki (ki msg))
    (cl:cons ':kd (kd msg))
    (cl:cons ':controlMax (controlMax msg))
))
