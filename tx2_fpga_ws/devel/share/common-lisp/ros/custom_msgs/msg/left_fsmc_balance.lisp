; Auto-generated. Do not edit!


(cl:in-package custom_msgs-msg)


;//! \htmlinclude left_fsmc_balance.msg.html

(cl:defclass <left_fsmc_balance> (roslisp-msg-protocol:ros-message)
  ((lamda
    :reader lamda
    :initarg :lamda
    :type cl:float
    :initform 0.0)
   (gs1
    :reader gs1
    :initarg :gs1
    :type cl:float
    :initform 0.0)
   (gs2
    :reader gs2
    :initarg :gs2
    :type cl:float
    :initform 0.0)
   (gs3
    :reader gs3
    :initarg :gs3
    :type cl:float
    :initform 0.0)
   (gu1
    :reader gu1
    :initarg :gu1
    :type cl:float
    :initform 0.0)
   (gu2
    :reader gu2
    :initarg :gu2
    :type cl:float
    :initform 0.0)
   (gu3
    :reader gu3
    :initarg :gu3
    :type cl:float
    :initform 0.0)
   (controlMax
    :reader controlMax
    :initarg :controlMax
    :type cl:float
    :initform 0.0))
)

(cl:defclass left_fsmc_balance (<left_fsmc_balance>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <left_fsmc_balance>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'left_fsmc_balance)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name custom_msgs-msg:<left_fsmc_balance> is deprecated: use custom_msgs-msg:left_fsmc_balance instead.")))

(cl:ensure-generic-function 'lamda-val :lambda-list '(m))
(cl:defmethod lamda-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:lamda-val is deprecated.  Use custom_msgs-msg:lamda instead.")
  (lamda m))

(cl:ensure-generic-function 'gs1-val :lambda-list '(m))
(cl:defmethod gs1-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:gs1-val is deprecated.  Use custom_msgs-msg:gs1 instead.")
  (gs1 m))

(cl:ensure-generic-function 'gs2-val :lambda-list '(m))
(cl:defmethod gs2-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:gs2-val is deprecated.  Use custom_msgs-msg:gs2 instead.")
  (gs2 m))

(cl:ensure-generic-function 'gs3-val :lambda-list '(m))
(cl:defmethod gs3-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:gs3-val is deprecated.  Use custom_msgs-msg:gs3 instead.")
  (gs3 m))

(cl:ensure-generic-function 'gu1-val :lambda-list '(m))
(cl:defmethod gu1-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:gu1-val is deprecated.  Use custom_msgs-msg:gu1 instead.")
  (gu1 m))

(cl:ensure-generic-function 'gu2-val :lambda-list '(m))
(cl:defmethod gu2-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:gu2-val is deprecated.  Use custom_msgs-msg:gu2 instead.")
  (gu2 m))

(cl:ensure-generic-function 'gu3-val :lambda-list '(m))
(cl:defmethod gu3-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:gu3-val is deprecated.  Use custom_msgs-msg:gu3 instead.")
  (gu3 m))

(cl:ensure-generic-function 'controlMax-val :lambda-list '(m))
(cl:defmethod controlMax-val ((m <left_fsmc_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:controlMax-val is deprecated.  Use custom_msgs-msg:controlMax instead.")
  (controlMax m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <left_fsmc_balance>) ostream)
  "Serializes a message object of type '<left_fsmc_balance>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'lamda))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gs1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gs2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gs3))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gu1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gu2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'gu3))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'controlMax))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <left_fsmc_balance>) istream)
  "Deserializes a message object of type '<left_fsmc_balance>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'lamda) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gs1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gs2) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gs3) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gu1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gu2) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'gu3) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'controlMax) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<left_fsmc_balance>)))
  "Returns string type for a message object of type '<left_fsmc_balance>"
  "custom_msgs/left_fsmc_balance")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'left_fsmc_balance)))
  "Returns string type for a message object of type 'left_fsmc_balance"
  "custom_msgs/left_fsmc_balance")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<left_fsmc_balance>)))
  "Returns md5sum for a message object of type '<left_fsmc_balance>"
  "dcef653360ac32e30e928872b8672b09")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'left_fsmc_balance)))
  "Returns md5sum for a message object of type 'left_fsmc_balance"
  "dcef653360ac32e30e928872b8672b09")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<left_fsmc_balance>)))
  "Returns full string definition for message of type '<left_fsmc_balance>"
  (cl:format cl:nil "float32 lamda~%float32 gs1~%float32 gs2~%float32 gs3~%float32 gu1~%float32 gu2~%float32 gu3~%float32 controlMax~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'left_fsmc_balance)))
  "Returns full string definition for message of type 'left_fsmc_balance"
  (cl:format cl:nil "float32 lamda~%float32 gs1~%float32 gs2~%float32 gs3~%float32 gu1~%float32 gu2~%float32 gu3~%float32 controlMax~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <left_fsmc_balance>))
  (cl:+ 0
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <left_fsmc_balance>))
  "Converts a ROS message object to a list"
  (cl:list 'left_fsmc_balance
    (cl:cons ':lamda (lamda msg))
    (cl:cons ':gs1 (gs1 msg))
    (cl:cons ':gs2 (gs2 msg))
    (cl:cons ':gs3 (gs3 msg))
    (cl:cons ':gu1 (gu1 msg))
    (cl:cons ':gu2 (gu2 msg))
    (cl:cons ':gu3 (gu3 msg))
    (cl:cons ':controlMax (controlMax msg))
))
