; Auto-generated. Do not edit!


(cl:in-package custom_msgs-msg)


;//! \htmlinclude pendulum_status.msg.html

(cl:defclass <pendulum_status> (roslisp-msg-protocol:ros-message)
  ((time
    :reader time
    :initarg :time
    :type cl:float
    :initform 0.0)
   (angle_acc
    :reader angle_acc
    :initarg :angle_acc
    :type cl:float
    :initform 0.0)
   (angle_gyro
    :reader angle_gyro
    :initarg :angle_gyro
    :type cl:float
    :initform 0.0)
   (angle_filter
    :reader angle_filter
    :initarg :angle_filter
    :type cl:float
    :initform 0.0)
   (scnt
    :reader scnt
    :initarg :scnt
    :type cl:float
    :initform 0.0)
   (swave
    :reader swave
    :initarg :swave
    :type cl:float
    :initform 0.0)
   (pwmVal
    :reader pwmVal
    :initarg :pwmVal
    :type cl:fixnum
    :initform 0)
   (refOdm
    :reader refOdm
    :initarg :refOdm
    :type cl:float
    :initform 0.0)
   (rpm_L
    :reader rpm_L
    :initarg :rpm_L
    :type cl:fixnum
    :initform 0)
   (rpm_R
    :reader rpm_R
    :initarg :rpm_R
    :type cl:fixnum
    :initform 0)
   (rpm_L_tmp
    :reader rpm_L_tmp
    :initarg :rpm_L_tmp
    :type cl:fixnum
    :initform 0)
   (rpm_R_tmp
    :reader rpm_R_tmp
    :initarg :rpm_R_tmp
    :type cl:fixnum
    :initform 0)
   (dis_L_tmp
    :reader dis_L_tmp
    :initarg :dis_L_tmp
    :type cl:integer
    :initform 0)
   (dis_R_tmp
    :reader dis_R_tmp
    :initarg :dis_R_tmp
    :type cl:integer
    :initform 0)
   (odm_L
    :reader odm_L
    :initarg :odm_L
    :type cl:float
    :initform 0.0)
   (odm_R
    :reader odm_R
    :initarg :odm_R
    :type cl:float
    :initform 0.0)
   (locating
    :reader locating
    :initarg :locating
    :type cl:boolean
    :initform cl:nil)
   (vel_L
    :reader vel_L
    :initarg :vel_L
    :type cl:float
    :initform 0.0)
   (vel_R
    :reader vel_R
    :initarg :vel_R
    :type cl:float
    :initform 0.0)
   (vel_L_tmp
    :reader vel_L_tmp
    :initarg :vel_L_tmp
    :type cl:integer
    :initform 0)
   (vel_R_tmp
    :reader vel_R_tmp
    :initarg :vel_R_tmp
    :type cl:integer
    :initform 0)
   (switch_count_pulse_L
    :reader switch_count_pulse_L
    :initarg :switch_count_pulse_L
    :type cl:fixnum
    :initform 0)
   (switch_count_pulse_R
    :reader switch_count_pulse_R
    :initarg :switch_count_pulse_R
    :type cl:fixnum
    :initform 0)
   (LrefV
    :reader LrefV
    :initarg :LrefV
    :type cl:fixnum
    :initform 0)
   (RrefV
    :reader RrefV
    :initarg :RrefV
    :type cl:fixnum
    :initform 0)
   (LerrV
    :reader LerrV
    :initarg :LerrV
    :type cl:float
    :initform 0.0)
   (RerrV
    :reader RerrV
    :initarg :RerrV
    :type cl:float
    :initform 0.0)
   (LcontrolV
    :reader LcontrolV
    :initarg :LcontrolV
    :type cl:float
    :initform 0.0)
   (RcontrolV
    :reader RcontrolV
    :initarg :RcontrolV
    :type cl:float
    :initform 0.0)
   (total_ctrl_L
    :reader total_ctrl_L
    :initarg :total_ctrl_L
    :type cl:float
    :initform 0.0)
   (total_ctrl_R
    :reader total_ctrl_R
    :initarg :total_ctrl_R
    :type cl:float
    :initform 0.0)
   (LRrefB
    :reader LRrefB
    :initarg :LRrefB
    :type cl:float
    :initform 0.0)
   (LRerrB
    :reader LRerrB
    :initarg :LRerrB
    :type cl:float
    :initform 0.0)
   (LcontrolB
    :reader LcontrolB
    :initarg :LcontrolB
    :type cl:float
    :initform 0.0)
   (RcontrolB
    :reader RcontrolB
    :initarg :RcontrolB
    :type cl:float
    :initform 0.0))
)

(cl:defclass pendulum_status (<pendulum_status>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <pendulum_status>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'pendulum_status)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name custom_msgs-msg:<pendulum_status> is deprecated: use custom_msgs-msg:pendulum_status instead.")))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:time-val is deprecated.  Use custom_msgs-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'angle_acc-val :lambda-list '(m))
(cl:defmethod angle_acc-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:angle_acc-val is deprecated.  Use custom_msgs-msg:angle_acc instead.")
  (angle_acc m))

(cl:ensure-generic-function 'angle_gyro-val :lambda-list '(m))
(cl:defmethod angle_gyro-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:angle_gyro-val is deprecated.  Use custom_msgs-msg:angle_gyro instead.")
  (angle_gyro m))

(cl:ensure-generic-function 'angle_filter-val :lambda-list '(m))
(cl:defmethod angle_filter-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:angle_filter-val is deprecated.  Use custom_msgs-msg:angle_filter instead.")
  (angle_filter m))

(cl:ensure-generic-function 'scnt-val :lambda-list '(m))
(cl:defmethod scnt-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:scnt-val is deprecated.  Use custom_msgs-msg:scnt instead.")
  (scnt m))

(cl:ensure-generic-function 'swave-val :lambda-list '(m))
(cl:defmethod swave-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:swave-val is deprecated.  Use custom_msgs-msg:swave instead.")
  (swave m))

(cl:ensure-generic-function 'pwmVal-val :lambda-list '(m))
(cl:defmethod pwmVal-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:pwmVal-val is deprecated.  Use custom_msgs-msg:pwmVal instead.")
  (pwmVal m))

(cl:ensure-generic-function 'refOdm-val :lambda-list '(m))
(cl:defmethod refOdm-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:refOdm-val is deprecated.  Use custom_msgs-msg:refOdm instead.")
  (refOdm m))

(cl:ensure-generic-function 'rpm_L-val :lambda-list '(m))
(cl:defmethod rpm_L-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:rpm_L-val is deprecated.  Use custom_msgs-msg:rpm_L instead.")
  (rpm_L m))

(cl:ensure-generic-function 'rpm_R-val :lambda-list '(m))
(cl:defmethod rpm_R-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:rpm_R-val is deprecated.  Use custom_msgs-msg:rpm_R instead.")
  (rpm_R m))

(cl:ensure-generic-function 'rpm_L_tmp-val :lambda-list '(m))
(cl:defmethod rpm_L_tmp-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:rpm_L_tmp-val is deprecated.  Use custom_msgs-msg:rpm_L_tmp instead.")
  (rpm_L_tmp m))

(cl:ensure-generic-function 'rpm_R_tmp-val :lambda-list '(m))
(cl:defmethod rpm_R_tmp-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:rpm_R_tmp-val is deprecated.  Use custom_msgs-msg:rpm_R_tmp instead.")
  (rpm_R_tmp m))

(cl:ensure-generic-function 'dis_L_tmp-val :lambda-list '(m))
(cl:defmethod dis_L_tmp-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:dis_L_tmp-val is deprecated.  Use custom_msgs-msg:dis_L_tmp instead.")
  (dis_L_tmp m))

(cl:ensure-generic-function 'dis_R_tmp-val :lambda-list '(m))
(cl:defmethod dis_R_tmp-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:dis_R_tmp-val is deprecated.  Use custom_msgs-msg:dis_R_tmp instead.")
  (dis_R_tmp m))

(cl:ensure-generic-function 'odm_L-val :lambda-list '(m))
(cl:defmethod odm_L-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:odm_L-val is deprecated.  Use custom_msgs-msg:odm_L instead.")
  (odm_L m))

(cl:ensure-generic-function 'odm_R-val :lambda-list '(m))
(cl:defmethod odm_R-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:odm_R-val is deprecated.  Use custom_msgs-msg:odm_R instead.")
  (odm_R m))

(cl:ensure-generic-function 'locating-val :lambda-list '(m))
(cl:defmethod locating-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:locating-val is deprecated.  Use custom_msgs-msg:locating instead.")
  (locating m))

(cl:ensure-generic-function 'vel_L-val :lambda-list '(m))
(cl:defmethod vel_L-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:vel_L-val is deprecated.  Use custom_msgs-msg:vel_L instead.")
  (vel_L m))

(cl:ensure-generic-function 'vel_R-val :lambda-list '(m))
(cl:defmethod vel_R-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:vel_R-val is deprecated.  Use custom_msgs-msg:vel_R instead.")
  (vel_R m))

(cl:ensure-generic-function 'vel_L_tmp-val :lambda-list '(m))
(cl:defmethod vel_L_tmp-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:vel_L_tmp-val is deprecated.  Use custom_msgs-msg:vel_L_tmp instead.")
  (vel_L_tmp m))

(cl:ensure-generic-function 'vel_R_tmp-val :lambda-list '(m))
(cl:defmethod vel_R_tmp-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:vel_R_tmp-val is deprecated.  Use custom_msgs-msg:vel_R_tmp instead.")
  (vel_R_tmp m))

(cl:ensure-generic-function 'switch_count_pulse_L-val :lambda-list '(m))
(cl:defmethod switch_count_pulse_L-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:switch_count_pulse_L-val is deprecated.  Use custom_msgs-msg:switch_count_pulse_L instead.")
  (switch_count_pulse_L m))

(cl:ensure-generic-function 'switch_count_pulse_R-val :lambda-list '(m))
(cl:defmethod switch_count_pulse_R-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:switch_count_pulse_R-val is deprecated.  Use custom_msgs-msg:switch_count_pulse_R instead.")
  (switch_count_pulse_R m))

(cl:ensure-generic-function 'LrefV-val :lambda-list '(m))
(cl:defmethod LrefV-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:LrefV-val is deprecated.  Use custom_msgs-msg:LrefV instead.")
  (LrefV m))

(cl:ensure-generic-function 'RrefV-val :lambda-list '(m))
(cl:defmethod RrefV-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:RrefV-val is deprecated.  Use custom_msgs-msg:RrefV instead.")
  (RrefV m))

(cl:ensure-generic-function 'LerrV-val :lambda-list '(m))
(cl:defmethod LerrV-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:LerrV-val is deprecated.  Use custom_msgs-msg:LerrV instead.")
  (LerrV m))

(cl:ensure-generic-function 'RerrV-val :lambda-list '(m))
(cl:defmethod RerrV-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:RerrV-val is deprecated.  Use custom_msgs-msg:RerrV instead.")
  (RerrV m))

(cl:ensure-generic-function 'LcontrolV-val :lambda-list '(m))
(cl:defmethod LcontrolV-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:LcontrolV-val is deprecated.  Use custom_msgs-msg:LcontrolV instead.")
  (LcontrolV m))

(cl:ensure-generic-function 'RcontrolV-val :lambda-list '(m))
(cl:defmethod RcontrolV-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:RcontrolV-val is deprecated.  Use custom_msgs-msg:RcontrolV instead.")
  (RcontrolV m))

(cl:ensure-generic-function 'total_ctrl_L-val :lambda-list '(m))
(cl:defmethod total_ctrl_L-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:total_ctrl_L-val is deprecated.  Use custom_msgs-msg:total_ctrl_L instead.")
  (total_ctrl_L m))

(cl:ensure-generic-function 'total_ctrl_R-val :lambda-list '(m))
(cl:defmethod total_ctrl_R-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:total_ctrl_R-val is deprecated.  Use custom_msgs-msg:total_ctrl_R instead.")
  (total_ctrl_R m))

(cl:ensure-generic-function 'LRrefB-val :lambda-list '(m))
(cl:defmethod LRrefB-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:LRrefB-val is deprecated.  Use custom_msgs-msg:LRrefB instead.")
  (LRrefB m))

(cl:ensure-generic-function 'LRerrB-val :lambda-list '(m))
(cl:defmethod LRerrB-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:LRerrB-val is deprecated.  Use custom_msgs-msg:LRerrB instead.")
  (LRerrB m))

(cl:ensure-generic-function 'LcontrolB-val :lambda-list '(m))
(cl:defmethod LcontrolB-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:LcontrolB-val is deprecated.  Use custom_msgs-msg:LcontrolB instead.")
  (LcontrolB m))

(cl:ensure-generic-function 'RcontrolB-val :lambda-list '(m))
(cl:defmethod RcontrolB-val ((m <pendulum_status>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:RcontrolB-val is deprecated.  Use custom_msgs-msg:RcontrolB instead.")
  (RcontrolB m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <pendulum_status>) ostream)
  "Serializes a message object of type '<pendulum_status>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angle_acc))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angle_gyro))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'angle_filter))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'scnt))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'swave))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'pwmVal)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'refOdm))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'rpm_L)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'rpm_R)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'rpm_L_tmp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'rpm_R_tmp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'dis_L_tmp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'dis_R_tmp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'odm_L))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'odm_R))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'locating) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vel_L))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vel_R))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'vel_L_tmp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'vel_R_tmp)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'switch_count_pulse_L)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'switch_count_pulse_R)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'LrefV)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'RrefV)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'LerrV))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'RerrV))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'LcontrolV))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'RcontrolV))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'total_ctrl_L))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'total_ctrl_R))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'LRrefB))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'LRerrB))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'LcontrolB))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'RcontrolB))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <pendulum_status>) istream)
  "Deserializes a message object of type '<pendulum_status>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'time) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angle_acc) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angle_gyro) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'angle_filter) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'scnt) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'swave) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'pwmVal) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'refOdm) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rpm_L) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rpm_R) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rpm_L_tmp) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rpm_R_tmp) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'dis_L_tmp) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'dis_R_tmp) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'odm_L) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'odm_R) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'locating) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vel_L) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vel_R) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'vel_L_tmp) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'vel_R_tmp) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'switch_count_pulse_L) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'switch_count_pulse_R) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'LrefV) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'RrefV) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'LerrV) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'RerrV) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'LcontrolV) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'RcontrolV) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'total_ctrl_L) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'total_ctrl_R) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'LRrefB) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'LRerrB) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'LcontrolB) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'RcontrolB) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<pendulum_status>)))
  "Returns string type for a message object of type '<pendulum_status>"
  "custom_msgs/pendulum_status")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'pendulum_status)))
  "Returns string type for a message object of type 'pendulum_status"
  "custom_msgs/pendulum_status")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<pendulum_status>)))
  "Returns md5sum for a message object of type '<pendulum_status>"
  "0901b4abd021ac391383299961900100")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'pendulum_status)))
  "Returns md5sum for a message object of type 'pendulum_status"
  "0901b4abd021ac391383299961900100")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<pendulum_status>)))
  "Returns full string definition for message of type '<pendulum_status>"
  (cl:format cl:nil "float64 time~%~%float32 angle_acc~%float32 angle_gyro~%float32 angle_filter~%~%float32 scnt~%float32 swave~%int16 pwmVal~%float32 refOdm~%~%int16 rpm_L~%int16 rpm_R~%int16 rpm_L_tmp~%int16 rpm_R_tmp~%int32 dis_L_tmp~%int32 dis_R_tmp~%float32 odm_L~%float32 odm_R~%bool locating~%float32 vel_L~%float32 vel_R~%int32 vel_L_tmp~%int32 vel_R_tmp~%int16 switch_count_pulse_L~%int16 switch_count_pulse_R~%~%int16 LrefV~%int16 RrefV~%float32 LerrV~%float32 RerrV~%float32 LcontrolV~%float32 RcontrolV~%float32 total_ctrl_L~%float32 total_ctrl_R~%~%float32 LRrefB~%float32 LRerrB~%float32 LcontrolB~%float32 RcontrolB~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'pendulum_status)))
  "Returns full string definition for message of type 'pendulum_status"
  (cl:format cl:nil "float64 time~%~%float32 angle_acc~%float32 angle_gyro~%float32 angle_filter~%~%float32 scnt~%float32 swave~%int16 pwmVal~%float32 refOdm~%~%int16 rpm_L~%int16 rpm_R~%int16 rpm_L_tmp~%int16 rpm_R_tmp~%int32 dis_L_tmp~%int32 dis_R_tmp~%float32 odm_L~%float32 odm_R~%bool locating~%float32 vel_L~%float32 vel_R~%int32 vel_L_tmp~%int32 vel_R_tmp~%int16 switch_count_pulse_L~%int16 switch_count_pulse_R~%~%int16 LrefV~%int16 RrefV~%float32 LerrV~%float32 RerrV~%float32 LcontrolV~%float32 RcontrolV~%float32 total_ctrl_L~%float32 total_ctrl_R~%~%float32 LRrefB~%float32 LRerrB~%float32 LcontrolB~%float32 RcontrolB~%~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <pendulum_status>))
  (cl:+ 0
     8
     4
     4
     4
     4
     4
     2
     4
     2
     2
     2
     2
     4
     4
     4
     4
     1
     4
     4
     4
     4
     2
     2
     2
     2
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <pendulum_status>))
  "Converts a ROS message object to a list"
  (cl:list 'pendulum_status
    (cl:cons ':time (time msg))
    (cl:cons ':angle_acc (angle_acc msg))
    (cl:cons ':angle_gyro (angle_gyro msg))
    (cl:cons ':angle_filter (angle_filter msg))
    (cl:cons ':scnt (scnt msg))
    (cl:cons ':swave (swave msg))
    (cl:cons ':pwmVal (pwmVal msg))
    (cl:cons ':refOdm (refOdm msg))
    (cl:cons ':rpm_L (rpm_L msg))
    (cl:cons ':rpm_R (rpm_R msg))
    (cl:cons ':rpm_L_tmp (rpm_L_tmp msg))
    (cl:cons ':rpm_R_tmp (rpm_R_tmp msg))
    (cl:cons ':dis_L_tmp (dis_L_tmp msg))
    (cl:cons ':dis_R_tmp (dis_R_tmp msg))
    (cl:cons ':odm_L (odm_L msg))
    (cl:cons ':odm_R (odm_R msg))
    (cl:cons ':locating (locating msg))
    (cl:cons ':vel_L (vel_L msg))
    (cl:cons ':vel_R (vel_R msg))
    (cl:cons ':vel_L_tmp (vel_L_tmp msg))
    (cl:cons ':vel_R_tmp (vel_R_tmp msg))
    (cl:cons ':switch_count_pulse_L (switch_count_pulse_L msg))
    (cl:cons ':switch_count_pulse_R (switch_count_pulse_R msg))
    (cl:cons ':LrefV (LrefV msg))
    (cl:cons ':RrefV (RrefV msg))
    (cl:cons ':LerrV (LerrV msg))
    (cl:cons ':RerrV (RerrV msg))
    (cl:cons ':LcontrolV (LcontrolV msg))
    (cl:cons ':RcontrolV (RcontrolV msg))
    (cl:cons ':total_ctrl_L (total_ctrl_L msg))
    (cl:cons ':total_ctrl_R (total_ctrl_R msg))
    (cl:cons ':LRrefB (LRrefB msg))
    (cl:cons ':LRerrB (LRerrB msg))
    (cl:cons ':LcontrolB (LcontrolB msg))
    (cl:cons ':RcontrolB (RcontrolB msg))
))
