; Auto-generated. Do not edit!


(cl:in-package custom_msgs-msg)


;//! \htmlinclude right_pid_balance.msg.html

(cl:defclass <right_pid_balance> (roslisp-msg-protocol:ros-message)
  ((kp
    :reader kp
    :initarg :kp
    :type cl:float
    :initform 0.0)
   (ki
    :reader ki
    :initarg :ki
    :type cl:float
    :initform 0.0)
   (kd
    :reader kd
    :initarg :kd
    :type cl:float
    :initform 0.0)
   (controlMax
    :reader controlMax
    :initarg :controlMax
    :type cl:float
    :initform 0.0))
)

(cl:defclass right_pid_balance (<right_pid_balance>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <right_pid_balance>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'right_pid_balance)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name custom_msgs-msg:<right_pid_balance> is deprecated: use custom_msgs-msg:right_pid_balance instead.")))

(cl:ensure-generic-function 'kp-val :lambda-list '(m))
(cl:defmethod kp-val ((m <right_pid_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:kp-val is deprecated.  Use custom_msgs-msg:kp instead.")
  (kp m))

(cl:ensure-generic-function 'ki-val :lambda-list '(m))
(cl:defmethod ki-val ((m <right_pid_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:ki-val is deprecated.  Use custom_msgs-msg:ki instead.")
  (ki m))

(cl:ensure-generic-function 'kd-val :lambda-list '(m))
(cl:defmethod kd-val ((m <right_pid_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:kd-val is deprecated.  Use custom_msgs-msg:kd instead.")
  (kd m))

(cl:ensure-generic-function 'controlMax-val :lambda-list '(m))
(cl:defmethod controlMax-val ((m <right_pid_balance>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msgs-msg:controlMax-val is deprecated.  Use custom_msgs-msg:controlMax instead.")
  (controlMax m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <right_pid_balance>) ostream)
  "Serializes a message object of type '<right_pid_balance>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'kp))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ki))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'kd))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'controlMax))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <right_pid_balance>) istream)
  "Deserializes a message object of type '<right_pid_balance>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'kp) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ki) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'kd) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'controlMax) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<right_pid_balance>)))
  "Returns string type for a message object of type '<right_pid_balance>"
  "custom_msgs/right_pid_balance")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'right_pid_balance)))
  "Returns string type for a message object of type 'right_pid_balance"
  "custom_msgs/right_pid_balance")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<right_pid_balance>)))
  "Returns md5sum for a message object of type '<right_pid_balance>"
  "623d8ff803e92a174b2a9e53a920d15c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'right_pid_balance)))
  "Returns md5sum for a message object of type 'right_pid_balance"
  "623d8ff803e92a174b2a9e53a920d15c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<right_pid_balance>)))
  "Returns full string definition for message of type '<right_pid_balance>"
  (cl:format cl:nil "float32 kp~%float32 ki~%float32 kd~%float32 controlMax~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'right_pid_balance)))
  "Returns full string definition for message of type 'right_pid_balance"
  (cl:format cl:nil "float32 kp~%float32 ki~%float32 kd~%float32 controlMax~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <right_pid_balance>))
  (cl:+ 0
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <right_pid_balance>))
  "Converts a ROS message object to a list"
  (cl:list 'right_pid_balance
    (cl:cons ':kp (kp msg))
    (cl:cons ':ki (ki msg))
    (cl:cons ':kd (kd msg))
    (cl:cons ':controlMax (controlMax msg))
))
