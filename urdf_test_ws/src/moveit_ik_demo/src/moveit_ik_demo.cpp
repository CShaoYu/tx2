#include <string>
#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <tf/transform_datatypes.h>


//到時候看時間再決定要步要分開打左右及一個兩手互動的策略
int main(int argc, char **argv)
{
    
    ros::init(argc, argv, "moveit_fk_demo");
    
    ros::AsyncSpinner spinner(1);
    
    spinner.start();

    tf::Quaternion q;
    q.setEulerZYX(0.0, 0.0, 0.0);
    ROS_INFO("orientation.x %f",q.x());
    ROS_INFO("orientation.y %f",q.y());
    ROS_INFO("orientation.z %f",q.z());
    ROS_INFO("orientation.w %f",q.w());

    static const std::string PLANNING_GROUP_L = "armL";
    static const std::string PLANNING_GROUP_R = "armR";

    
    moveit::planning_interface::MoveGroupInterface armL(PLANNING_GROUP_L);
    moveit::planning_interface::MoveGroupInterface armR(PLANNING_GROUP_R);

    // Raw pointers are frequently used to refer to the planning group for improved performance.
    // 看起來只是為了畫軌跡用的
    const robot_state::JointModelGroup* joint_model_group_L =
        armL.getCurrentState()->getJointModelGroup(PLANNING_GROUP_L); 
    const robot_state::JointModelGroup* joint_model_group_R =
        armR.getCurrentState()->getJointModelGroup(PLANNING_GROUP_R); 

    namespace rvt = rviz_visual_tools;
    moveit_visual_tools::MoveItVisualTools visual_tools("base_link");
    visual_tools.deleteAllMarkers();

    visual_tools.loadRemoteControl();

    Eigen::Affine3d text_pose = Eigen::Affine3d::Identity();
    text_pose.translation().z() = 1.75;
    visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);
    visual_tools.trigger();

    ROS_INFO_NAMED("tutorial", "Reference frame: %s", armL.getPlanningFrame().c_str());
    ROS_INFO_NAMED("tutorial", "Reference frame: %s", armR.getPlanningFrame().c_str());
    ROS_INFO_NAMED("tutorial", "End effector link: %s", armL.getEndEffectorLink().c_str());
    ROS_INFO_NAMED("tutorial", "End effector link: %s", armR.getEndEffectorLink().c_str());

    sleep(0.5);

    geometry_msgs::Pose target_pose_L, target_pose_R;
    target_pose_L.orientation.x = q.x();
    target_pose_L.orientation.y = q.y();
    target_pose_L.orientation.z = q.z();
    target_pose_L.orientation.w = q.w();
    armL.setPoseTarget(target_pose_L);
    target_pose_R.orientation.x = 0.417122;
    target_pose_R.orientation.y = 0.584361;
    target_pose_R.orientation.z = -0.496482;
    target_pose_R.orientation.w = -0.48789;
    armR.setPoseTarget(target_pose_R);

    moveit::planning_interface::MoveGroupInterface::Plan plan_L, plan_R;
    moveit::planning_interface::MoveItErrorCode successL = armL.plan(plan_L);
    moveit::planning_interface::MoveItErrorCode successR = armR.plan(plan_R);
    ROS_INFO("Plan (pose goal) %s",successL?"":"FAILED");   
    ROS_INFO("Plan (pose goal) %s",successR?"":"FAILED");

    ROS_INFO_NAMED("tutorial", "Visualizing plan 1 as trajectory line");
    visual_tools.publishAxisLabeled(target_pose_L, "pose1_L");
    visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishTrajectoryLine(plan_L.trajectory_, joint_model_group_L);
    visual_tools.trigger();
    ROS_INFO_NAMED("tutorial", "Visualizing plan 1 as trajectory line");
    visual_tools.publishAxisLabeled(target_pose_R, "pose1_R");
    visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishTrajectoryLine(plan_R.trajectory_, joint_model_group_R);
    visual_tools.trigger();
    visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");
    sleep(1);

    // Planning to a joint-space goal
    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    //
    // Let's set a joint space goal and move towards it.  This will replace the
    // pose target we set above.
    //
    // To start, we'll create an pointer that references the current robot's state.
    // RobotState is the object that contains all the current position/velocity/acceleration data.
    moveit::core::RobotStatePtr current_state_L = armL.getCurrentState();
    moveit::core::RobotStatePtr current_state_R = armR.getCurrentState();
    //
    // Next get the current set of joint values for the group.
    std::vector<double> joint_group_positions_L;
    current_state_L->copyJointGroupPositions(joint_model_group_L, joint_group_positions_L);
    std::vector<double> joint_group_positions_R;
    current_state_R->copyJointGroupPositions(joint_model_group_R, joint_group_positions_R);

    // Now, let's modify one of the joints, plan to the new joint space goal and visualize the plan.
    joint_group_positions_L[0] = 1.57;  // radians  //左手第一軸
    armL.setJointValueTarget(joint_group_positions_L);    
    joint_group_positions_R[1] = -1.57;  // radians  //右手第二軸
    armR.setJointValueTarget(joint_group_positions_R);

    successL = armL.plan(plan_L);  //會開始模擬(plan)
    ROS_INFO("Plan (pose goal) %s",successL?"":"FAILED"); 
    sleep(5);  //sec
    successR = armR.plan(plan_R);      
    ROS_INFO("Plan (pose goal) %s",successR?"":"FAILED");
    sleep(5);  //sec

    // Visualize the plan in RViz
    visual_tools.deleteAllMarkers();
    visual_tools.publishAxisLabeled(target_pose_L, "pose1_L");
    visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishTrajectoryLine(plan_L.trajectory_, joint_model_group_L);
    visual_tools.trigger();
    visual_tools.publishAxisLabeled(target_pose_R, "pose1_R");
    visual_tools.publishText(text_pose, "Pose Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishTrajectoryLine(plan_R.trajectory_, joint_model_group_R);
    visual_tools.trigger();
    //visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");
    sleep(1);


    
    //std::string end_effector_linkL = armL.getEndEffectorLink();
    //std::string end_effector_linkR = armR.getEndEffectorLink();
    
    // std::string reference_frame = "base_link";
    // armL.setPoseReferenceFrame(reference_frame);
    // armR.setPoseReferenceFrame(reference_frame);
    
    // armL.allowReplanning(true);
    // armR.allowReplanning(true);
   
    // armL.setGoalPositionTolerance(0.001);
    // armL.setGoalOrientationTolerance(0.01);
    // armR.setGoalPositionTolerance(0.001);
    // armR.setGoalOrientationTolerance(0.01);

    // armL.setMaxAccelerationScalingFactor(0.2);
    // armL.setMaxVelocityScalingFactor(0.2);
    // armR.setMaxAccelerationScalingFactor(0.2);
    // armR.setMaxVelocityScalingFactor(0.2);
    
    // armL.setNamedTarget("init_poseL");    
    // armL.move(); 
    // armR.setNamedTarget("init_poseR");
    // armR.move(); 
    // sleep(1); 


    // geometry_msgs::Pose target_pose;

    // target_pose.orientation.x = 2.02848e-05;
    // target_pose.orientation.y = -0.382698;
    // target_pose.orientation.z = 1.05249e-05;
    // target_pose.orientation.w = 0.923874;
  
    // target_pose.position.x = -0.142438;
    // target_pose.position.y = 0.279995;
    // target_pose.position.z = -0.0410153;
    
    // armL.setStartStateToCurrentState();
    // armL.setPoseTarget(target_poseL);
    // armR.setStartStateToCurrentState();
    // armR.setPoseTarget(target_pose);

    ros::shutdown(); 

    return 0;
}
