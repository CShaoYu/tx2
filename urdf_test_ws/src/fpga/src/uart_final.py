#!usr/bin/python3.5

import numpy as np
import sys
import serial
from time import sleep


COM_PORT = '/dev/ttyUSB0'
BAUD_RATES = 115200
ser = serial.Serial(COM_PORT, BAUD_RATES)
#ser.open()  #already open
'''
filename = '/home/pi/uart_test_py/3Dcoordinate.txt'

elements = []
with open(filename) as file:
 for line in file:
  line = line.strip().split()
  elements.append(line)
print(elements)
int = 1
float = 1.23456
sstr = 'a'
str_num = '10'
str2 = '1234'
str3 = '1234.5678'
print(sys.getsizeof(int))
print(sys.getsizeof(float))
print(sys.getsizeof(sstr))
print(sys.getsizeof(str2))
print(sys.getsizeof(str3))
'''
#ser.close()
#sleep(2)
#ser.open()
#sleep(2)

'''
a = 123
a = float(a)  #123.0
a = round(a, 2) #取小數點後兩位 123.0
str_a = str(a) #'123.0'
float_index = str_a.find('.') #3
str_float_index = str(float_index) #'3'
'''
uart_flag = 1

x = 123.12
x = float(x)
x = round(x, 2)
str_x = str(x)
print(str_x)
if (x>0):
  x_size = sys.getsizeof(str_x) -49  #''空是49 
  s_x = str(x_size)  
  print(s_x)
  plus_or_negative_x ='+'  
  print(plus_or_negative_x)  
  float_index_x = str_x.find('.') 
  str_float_index_x = str(float_index_x) 
  print(str_float_index_x)

elif (x<0):
  x_size = sys.getsizeof(str_x) -49-1 #多-1是扣負數, 在Python 3中，字符串都是Unicode，開銷是49字節 原文網址：https://kknews.cc/code/5r8opj8.html
  s_x = str(x_size)
  print(s_x)  
  plus_or_negative_x ='-'
  print(plus_or_negative_x)
  float_index_x = str_x.find('.') -1  #多-1是扣負數
  str_float_index_x = str(float_index_x)
  print(str_float_index_x)
  str_x = str(-x)  #確認正負及位數後傳正數回去
  

y = -45.6267
y = float(y)
y = round(y, 2)
str_y = str(y)
print(str_y)
if (y>0):
  y_size = sys.getsizeof(str_y) -49
  s_y = str(y_size)
  print(s_y)
  plus_or_negative_y ='+'
  print(plus_or_negative_y)
  float_index_y = str_y.find('.') 
  str_float_index_y = str(float_index_y)
  print(str_float_index_y)
  
elif (y<0):
  y_size = sys.getsizeof(str_y) -49-1
  s_y = str(y_size)
  print(s_y)  
  plus_or_negative_y ='-'
  print(plus_or_negative_y)
  float_index_y = str_y.find('.') -1
  str_float_index_y = str(float_index_y)
  print(str_float_index_y)
  str_y = str(-y)
  

z = -789.1
z = float(z)
z = round(z, 2)
str_z = str(z)
print(str_z)
if (z>0):
  z_size = sys.getsizeof(str_z) -49
  s_z = str(z_size)
  print(s_z)
  plus_or_negative_z ='+'
  print(plus_or_negative_z)
  float_index_z = str_z.find('.') 
  str_float_index_z = str(float_index_z)
  print(str_float_index_z)
  
elif (z<0):
  z_size = sys.getsizeof(str_z) -49-1
  s_z = str(z_size)
  print(s_z)  
  plus_or_negative_z ='-'
  print(plus_or_negative_z)
  float_index_z = str_z.find('.') -1
  str_float_index_z = str(float_index_z)
  print(str_float_index_z)
  str_z = str(-z)
  

while(uart_flag):
    #sleep(3)
    '''
    ser.write(s_x.encode())
    #sleep(0.5)
    ser.write(plus_or_negative_x.encode())
    ser.write(str_float_index_x.encode())
    ser.write(str_x.encode())
    sleep(0.5)
    '''
    ser.write(s_y.encode())
    #sleep(5)
    ser.write(plus_or_negative_y.encode())
    ser.write(str_float_index_y.encode())
    ser.write(str_y.encode())
    sleep(0.5)    
    '''   
    ser.write(s_z.encode())
    #sleep(1.2)
    ser.write(plus_or_negative_z.encode())
    ser.write(str_float_index_z.encode())
    ser.write(str_z.encode())
    sleep(0.5)
    '''

    uart_flag = 0;
   
   # while True:
        #print("start")
       
 #ser.write(str_num.encode())
        #int += int
ser.close()
print('bye！')










