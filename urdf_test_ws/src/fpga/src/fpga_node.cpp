
#include <ros/ros.h>
#include "sensor_msgs/JointState.h"
#include <string>
#include <iostream>
#include <cstdio>
#include <unistd.h>
#include "serial/serial.h"
#define PI 3.14159265358979323846  
#define DATASIZE 6

std::string port("/dev/ttyUSB0");
unsigned long baud = 115200;
serial::Serial my_serial(port, baud, serial::Timeout::simpleTimeout(1000));
 
size_t bytes_wrote; 
float refVal_list[2][6] = {0.0};
unsigned char uart_transmit_list[91];  //(2 * 5 * 9 ) +1, 2 arm, 5 joint/arm, 1 joint degree/9 unit

double rounding(double, int);
void chatterCallback(const sensor_msgs::JointState &msg)
{
  double theta;  //degree 
  for (int arm_No = 1; arm_No >= 0; arm_No--)
  {
    for (int joint_No = 1; joint_No <=5; joint_No++)
    {
      if (arm_No == 1)
      {        
        theta = (180.0*msg.position[joint_No-1])/PI;  //rad->degree
        theta = rounding(theta, 2);  //取小數點後兩位
        refVal_list[arm_No][joint_No] = theta;
      }
      else
      {
        theta = (180.0*msg.position[joint_No+4])/PI;
        theta = rounding(theta, 3);  //取小數點後兩位
        refVal_list[arm_No][joint_No] = theta;
      }
      ROS_INFO("refVal_list[%d][%d] %f", arm_No, joint_No, refVal_list[arm_No][joint_No]);
    }
  }

  char buffer[6];  //最多6byte(含小數點) XXX.XX, 不考慮+-的情況下
  size_t length = 0;
  char clength = 0;
  char *pch;
  char cplus_or_negative = '0';
  long int float_index = 0;
  char cfloat_index = '0';
  memset(buffer,0,sizeof(buffer));
  memset(uart_transmit_list,0,sizeof(uart_transmit_list));
  int scan_idx = 0;
  for (int arm_No = 0; arm_No <= 1; arm_No++)
  {
    for (int joint_No = 1; joint_No <= 5; joint_No++)
    {      
      if (refVal_list[arm_No][joint_No] > 0.0)      
        cplus_or_negative = '+';      
      else if (refVal_list[arm_No][joint_No] < 0.0)      
        cplus_or_negative = '-';
      ROS_INFO("plus_or_negative %c", cplus_or_negative);

      snprintf(buffer, sizeof(buffer), "%f", fabs(refVal_list[arm_No][joint_No]));
      ROS_INFO("buffer %s", buffer);

      length = sizeof(buffer)/sizeof(char);  //如果想改成char* , https://stackoverflow.com/questions/21022644/how-to-get-the-real-and-total-length-of-char-char-array
      ROS_INFO("legnth %zu", length);
      clength = (char)length + 48;
      ROS_INFO("clegnth %c", clength);
                   
      pch=strchr(buffer,'.');
      while (pch!=NULL)
      {
        float_index = pch - buffer;
        ROS_INFO("float index %ld", float_index);      
        cfloat_index = (char)float_index + 48;
        ROS_INFO("float index char %c", cfloat_index);        
        pch=strchr(pch+1,'.');
      }

       uart_transmit_list[scan_idx] = clength;
       ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
       scan_idx += 1;
       uart_transmit_list[scan_idx] = cplus_or_negative;
       ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
       scan_idx += 1;
       uart_transmit_list[scan_idx] = cfloat_index;
       ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
       scan_idx += 1;

      for(int i = 0; i < DATASIZE; i++)
      {
        if (buffer[i] == '\0')
        {
          buffer[i] = '0';
        }
        uart_transmit_list[scan_idx] = buffer[i];
        ROS_INFO("uart_transmit_list %c, %d", uart_transmit_list[scan_idx], scan_idx);     
        scan_idx += 1;
      }
      ROS_INFO("uart_transmit_list \n");        
    }
  }  

  uart_transmit_list[scan_idx] = '!';
  ROS_INFO("uart_transmit_list end , %d\n", scan_idx);
  serial::Timeout to = serial::Timeout::simpleTimeout(1000);
  my_serial.setTimeout(to);
  //my_serial.setTimeout(serial::Timeout::max(), 0, 0, 1000, 0);
  bytes_wrote = my_serial.write(uart_transmit_list, 91);  
  
}



int main(int argc, char **argv)  
{
  ros::init(argc, argv, "fpga_node");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("joint_states", 1000, chatterCallback);
    
  std::cout << "Is the serial port open?";
  if(my_serial.isOpen())
    std::cout << " Yes." << std::endl;
  else
    std::cout << " No." << std::endl;

  ros::Rate loop_rate(10);
  while(ros::ok())
  {
    ros::spinOnce();

    loop_rate.sleep();
  }

  my_serial.close();
  std::cout << " close !!!!!!!!!!!!!!!." << std::endl;
  
  return 0;
}
// 四捨五入 取到 小數點第 X 位 
double rounding(double num, int index)
{
    bool isNegative = false; // whether is negative number or not
  
    if(num < 0) // if this number is negative, then convert to positive number
    {
        isNegative = true;  
        num = -num;
    }
  
    if(index >= 0)
    {
        int multiplier;
        multiplier = pow(10, index);
        num = (int)(num * multiplier + 0.5) / (multiplier * 1.0);
    }
  
    if(isNegative) // if this number is negative, then convert to negative number
    {
        num = -num;
    }
  
    return num;
}

